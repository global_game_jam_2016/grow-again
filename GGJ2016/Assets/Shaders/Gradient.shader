﻿Shader "Victor/Gradient"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ColorUp("Color Up", Color) = (1,1,1,1)
		_ColorDown("Color Down", Color) = (1,1,1,1)
		_Color("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			float4 _ColorUp;
			float4 _ColorDown;
			float4 _Color;

			struct appdata
			{
			    float4 v : POSITION;
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 v : TEXCOORD1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
//				o.v = v.vertex;
				o.v =  mul(_Object2World, v.vertex);
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			float4 frag (v2f i) : COLOR
			{
				// sample the texture
//				fixed4 col = tex2D(_MainTex, i.uv);
				float4 col;
				i.v = i.v*0.5+0.5;
				col = saturate(lerp(_ColorDown, _ColorUp, -(i.v.x+i.v.y)));
				
//				col = float4(i.v.xyz*0.5+0.5, 1);				
				return col * tex2D(_MainTex, i.uv) *_Color;
			}
			ENDCG
		}
	}
}
