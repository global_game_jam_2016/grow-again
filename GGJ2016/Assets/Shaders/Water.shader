﻿Shader "Victor/Water" {
        Properties {
            _MainTex ("Base (RGB)", 2D) = "white" {}
            _DispTex ("Disp Texture", 2D) = "gray" {}
            _Displacement ("Displacement", Range(0, 0.1)) = 0.5
			_Speed1("Wave Speed", Range(0, 0.1)) = 0.003
			_Speed2("Tint Speed", Range(0, 0.01)) = 0.003
            _Color ("Color", color) = (1,1,1,0)
        }
        SubShader {
				Tags
			{
				"RenderType" = "Opaque"
				"LightMode" = "ForwardBase"  // important!
											 // make sure that all uniforms are correctly set
			}
            LOD 300
            
				Pass{
				CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

				uniform float4 _LightColor0;



			sampler2D _DispTex;
			float4 _DispTex_ST;
			float _Displacement;

			sampler2D _MainTex;
			float4 _MainTex_ST;

			float _Speed1;
			float _Speed2;

			struct Input {
				float2 uv_MainTex;
				float2 uv_DispTex;
			};

			struct v2f {
				float4 pos : POSITION;
				half4 vertex : TEXCOORD0; 
				float2 uv_MainTex : TEXCOORD1;
				float2 uv_DispTex : TEXCOORD2;
				half3 normal : NORMAL;
			};

			v2f vert(appdata_full v) {
				v2f o;

				o.uv_MainTex = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.uv_DispTex = TRANSFORM_TEX(v.texcoord, _DispTex);

				o.uv_MainTex += float2(_Time.y, _Time.y) * _Speed1;
				o.uv_DispTex += float2(_Time.y, _Time.y) *_Speed2;

				v.vertex.xyz += v.normal * tex2Dlod(_DispTex, float4(o.uv_DispTex.xy, 0, 0)) * _Displacement;

				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

				return o;
			}

			



			fixed4 _Color;

			half4 frag(v2f i) : COLOR{
				half4 c = tex2D(_MainTex, i.uv_MainTex) * _Color;
				
				return c;
				//c = tex2Dlod(_DispTex, float4(i.uv_DispTex.xy, 0, 0));
				//return c.r;
			}
			ENDCG
				}
        }
        FallBack "Diffuse"
    }