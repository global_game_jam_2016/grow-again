﻿Shader "Victor/Backgroud"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ColorUp("Color Up", Color) = (1,1,1,1)
		_ColorDown("Color Down", Color) = (1,1,1,1)
		
		_Wave("Wave", 2D) = "white" {}
		
		_Color("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			
			#pragma target 3.0
			
			float4 _ColorUp;
			float4 _ColorDown;
			
			float4 _Color;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
				//UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _Wave;
			float4 _Wave_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv1 = TRANSFORM_TEX(v.uv1, _Wave);
				//UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			float4 frag (v2f i) : SV_Target
			{
				// sample the texture
				float4 col = tex2D(_MainTex, i.uv);
				float2 waveUV = i.uv1 * 0.3*(sin(_Time*0.2) +2);
				float4 wave = tex2D(_Wave, waveUV);
				col = lerp(_ColorDown, _ColorUp, i.uv.y);
				col = col * wave * _Color;
				
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);				
				return col;//float4(waveUV,0,1);
			}
			ENDCG
		}
	}
}
