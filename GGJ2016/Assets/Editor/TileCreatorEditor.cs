﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TileCreator))]
public class TileCreatorEditor: Editor {
	
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		TileCreator t = (TileCreator)target;
		
		t.setTypeIndex = EditorGUILayout.Popup("Type", t.setTypeIndex, TileTypes.tileTypes); 
		if (t.setTypeIndex != t.myTypeIndex){
			t.SetTileType(t.setTypeIndex);
		}
		
	}
}
