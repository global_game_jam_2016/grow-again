﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Cube))]
public class LevelBuilderEditor: Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		Cube c = (Cube)target;
		
		if(GUILayout.Button("Build"))
		{
			c.BuildCube();
		}
		
		if(GUILayout.Button("Destroy"))
		{
			c.DestroyCube();
		}
	}
}
