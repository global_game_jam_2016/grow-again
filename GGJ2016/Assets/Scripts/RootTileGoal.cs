﻿using UnityEngine;
using System.Collections;

public class RootTileGoal : Tile {

	RootTileDirt[] rootTiles;
	public static RootTileGoal instance;
	public bool isOpen = false;
	public Material mouthOpenMat;
	public Material mouthClosedMat;
	public MeshRenderer mouthRenderer;
	public GameObject halo;
	public ParticleSystem particles;
	
	void Start () {
		instance = this;
		rootTiles = GameObject.FindObjectsOfType<RootTileDirt>();
		canMoveOnto = false;
		
		mouthOpenMat = Resources.Load("MouthOpenMat") as Material;
		mouthClosedMat = Resources.Load("MouthClosedMat") as Material;
		
		mouthRenderer.material = mouthClosedMat;
	}
	
	public void CheckToOpen(){
		foreach (RootTileDirt t in rootTiles){
			if (!t.hasGrown){
				return;
			}
		}
		
		Open ();	
	}
	
	void Open(){
		isOpen = true;
		canMoveOnto = true;
		mouthRenderer.material = mouthOpenMat;
		RootManager.instance.GroundRoot(this);
		halo.SetActive(true);
		particles.Play();
	}
	
	public override void ConnectRoot(Root root){
		print ("WIN!");
		LevelManager.instance.RootWin();
	}
}
