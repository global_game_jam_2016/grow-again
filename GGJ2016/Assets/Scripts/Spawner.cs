﻿using UnityEngine;
using System.Collections;

public class Spawner : Tile {

	public GameObject spawnPrefab; 

	void Start () {
		GameObject spawn = Instantiate (spawnPrefab, transform.position, transform.rotation) as GameObject;
		spawn.transform.position += transform.up * (spawn.GetComponent<MeshRenderer>().bounds.size.z/2);
	}
	
	void Update () {
	
	}
}
