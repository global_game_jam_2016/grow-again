﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TreeGeneratorV3 : MonoBehaviour
{

	public GameObject branchPrefab;
	public GameObject leafPrefab;

	GameObject branch;
	GameObject leaf;

	public float branchScale = 0.2f;
	public float leafScale = 0.5f;

	Vector3 branchExtents;
	Vector3 leafExtents;

	Vector3[] direction = new Vector3[5];

	Vector3 pos0;
	Vector3 pos1;

	bool hasTrunk = false;

	public int branchNum = 3;
	public int branchIteration = 6;
	int oriIteration;

	public int branchHorizontalVariation = 3;
	public int branchVerticalVariation = 4;

	public float branchShrink = 0.9f;
	public float leafScaleVariation = 0.1f;
	public float trunkIncrease = 5;
	
	Vector3 SpawnBranch (Vector3 root, Vector3 dir)
	{
		GameObject newChunk = Instantiate (branch, new Vector3 (
			root.x + dir.x * branchExtents.x * 2, 
			root.y + dir.y * branchExtents.y * 2, 
			root.z + dir.z * branchExtents.z * 2), Quaternion.identity) as GameObject;
		
		
		newChunk.transform.position = root + (dir * branchExtents.x * 2);
		
		newChunk.transform.SetParent (transform);
		return newChunk.transform.position;
	}

	void SpawnLeaf (Vector3 root)
	{
		GameObject newChunk = Instantiate (leaf, new Vector3 (
			root.x, 
			root.y + branchExtents.y + leafExtents.y, 
			root.z), Quaternion.identity) as GameObject;
			
		newChunk.transform.position = root + (transform.up * leafExtents.y * 2);
		
		int vibrate = 6;
//		newChunk.transform.DOPunchScale (leaf.transform.localScale, 1f, vibrate - 1, 0.1f);
		newChunk.transform.SetParent (transform);
	}

	IEnumerator BranchOut (Vector3 root)
	{
		// first try
		Vector3 tempDir = direction [Random.Range (0, 4)]; // 0 1 2 3
		int length = Random.Range (branchIteration, branchIteration + branchHorizontalVariation);
		
		// along x direction
		if (tempDir == transform.right || tempDir == -transform.right) {
			tempDir = direction [Random.Range (2, 4)];
			for (int j = 0; j < length; j++) { //branch length
				root = SpawnBranch (root, tempDir);
				yield return new WaitForSeconds (0.05f);
			}
			if (branchIteration > 0) {
				StartCoroutine (BranchUp (root));
			} else {
				SpawnLeaf (root);
				yield return new WaitForSeconds (0.05f);
			}
		}
		
		// along z direction
		if (tempDir == transform.forward || tempDir == -transform.forward) {
			tempDir = direction [Random.Range (0, 2)];
			for (int j = 0; j< length; j++) { // branch length
				root = SpawnBranch (root, tempDir);
				yield return new WaitForSeconds (0.05f);
			}
			if (branchIteration > 0) {
				StartCoroutine (BranchUp (root));
			} else {
				SpawnLeaf (root);
				yield return new WaitForSeconds (0.05f);
			}
		}
	}

	IEnumerator BranchUp (Vector3 root)
	{
		// grow up
		for (int i=0; i<Random.Range (1, branchVerticalVariation); i++) { //1,2,3
			root = SpawnBranch (root, transform.up);
			yield return new WaitForSeconds (0.05f);
		}

		StartCoroutine (BranchOut (root));

		branchIteration -= 1;

		branch.transform.localScale *= branchShrink;
		branchExtents = branch.transform.localScale / 2f;

		float r = (Random.value * leafScaleVariation + 1);
		leaf.transform.localScale *= r;
	}

	IEnumerator Tree (Vector3 root)
	{
		if (hasTrunk == false) {
			for (int i=0; i<branchIteration+trunkIncrease; i++) { 
				root = SpawnBranch (root, transform.up);
				yield return new WaitForSeconds (0.05f);
			}
			hasTrunk = true;
			pos1 = root; // for new branches
		}
		StartCoroutine (BranchUp (root));
	}

	IEnumerator Grow ()
	{
		// reset
		Reset ();

		if (hasTrunk == false)
			StartCoroutine (Tree (pos0));
		else
			StartCoroutine (Tree (pos1));

		// time interval between branches
		yield return new WaitForSeconds (3.5f);

		branchNum -= 1;
		if (branchNum > 0) {
			StartCoroutine ("Grow");
		}
	}


	void Reset ()
	{
		branchIteration = oriIteration;
		
		branch.transform.localScale = new Vector3 (branchScale, branchScale, branchScale);
		leaf.transform.localScale = new Vector3 (leafScale, leafScale, leafScale);
		
		branchExtents = branch.transform.localScale / 2f;
		leafExtents = leaf.transform.localScale / 2f;
		
		pos0 = transform.position - new Vector3 (0, branchExtents.y * 2f, 0);
	}

	// Use this for initialization
	void Start ()
	{
		direction [0] = new Vector3 (-1, 0, 0);
		direction [1] = new Vector3 (1, 0, 0);
		direction [2] = new Vector3 (0, 0, -1);
		direction [3] = new Vector3 (0, 0, 1);
		direction [4] = new Vector3 (0, 1, 0);
		
		direction[0] = -transform.right;
		direction[1] = transform.right;
		direction[2] = -transform.forward;
		direction[3] = transform.forward;
		direction[4] = transform.up;
		
		oriIteration = branchIteration;

		branch = Instantiate (branchPrefab, new Vector3 (0, -100, 0), Quaternion.identity) as GameObject;
		leaf = Instantiate (leafPrefab, new Vector3 (0, -100, 0), Quaternion.identity) as GameObject;

		Reset ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Space)) {	
			StartCoroutine ("Grow");
		}

		if (Input.GetKeyDown (KeyCode.Q)) {	
			Application.LoadLevel (Application.loadedLevel);
		}
	}
}
