﻿using UnityEngine;
using System.Collections;

public class Obstacle : Tile {

	public Mesh[] rockMeshes;
	public GameObject rockObject;
	public bool chanceRock = true;
	[Range(0f, 100f)]
	public float rockChance = 50f;
	
	void Start () {
		
		bool doShowRock = true;
		if (chanceRock){
			float r = Random.Range(0f, 100f);
			if (r > rockChance){
				doShowRock = false;
			}
		}
		
		if (doShowRock){
			Mesh randomMesh = rockMeshes[Random.Range(0, rockMeshes.Length-1)];
			rockObject.GetComponentInChildren<MeshFilter>().mesh = randomMesh;
			rockObject.transform.Rotate(0, 0, Random.Range(0f, 360));
			float newScale = Random.Range(.6f, .85f);
			rockObject.transform.localScale = new Vector3(newScale, newScale, newScale);
		}
		
		MeshRenderer mr = rockObject.GetComponentInChildren<MeshRenderer>();
		if (mr){
			mr.enabled = doShowRock;
		}
	}
	
	public override void ConnectRoot(Root baseRoot){
		GetComponentInChildren<MeshRenderer>().material = Resources.Load("TileMat") as Material;
	}
	
	void Update () {
	
	}
}
