﻿using UnityEngine;
using System.Collections;

public class RootTileWater : Tile {

	public int addStrength = 5;
	
	void Start(){
		
	}
	
	public override void ConnectRoot(Root baseRoot){
		
		baseRoot.ChangeStrength(addStrength);
		RootManager.instance.GroundRoot(this);
	}
}
