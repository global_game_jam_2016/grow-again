﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class TileCreator : MonoBehaviour
{
		
	public string myType = TileTypes.tileTypes[0]; 
	
	[HideInInspector]
	public int myTypeIndex;
	
	[HideInInspector]
	public int setTypeIndex;
	
	public GameObject myTileObject;
	
	[HideInInspector]
	public GameObject myWall;
	
	[HideInInspector]
	public int myWallIndex;
	
	public GameObject defaultTile;
	
	public int row;
	public int col;
	public bool isEdge = false;
	
	void Start(){
		
		float offset = .1f;
		//tileVis.transform.position += tileVis.transform.up * Random.Range(-offset, offset);
	}
	
	#if UNITY_EDITOR	
	public void TileSetup(GameObject _defaultTile){
		defaultTile = _defaultTile;
		myTileObject = defaultTile;
		Tile tile = defaultTile.GetComponent<Tile>();
		
		if (tile){
			tile.row = row;
			tile.col = col;
			tile.isEdge = isEdge;
			tile.myWall = myWall;
			tile.myWallIndex = myWallIndex;
			tile.myType = "None";
			tile.tilePosition = transform.position;
			EditorUtility.SetDirty(tile);
		}
		
		EditorUtility.SetDirty (this);
	}
	
	public void SetTileType (int index)
	{
		if (myTileObject != null && myTileObject != defaultTile) {
			DestroyImmediate (myTileObject);
		}
		
		if (TileTypes.tileTypes [index] != "None") {
			
			myTypeIndex = index;
			myType = TileTypes.tileTypes [myTypeIndex];
			
			defaultTile.SetActive (false);
			myTileObject = PrefabUtility.InstantiatePrefab (Resources.Load ("Tiles/" + TileTypes.tileTypes [index])) as GameObject;
			
			Tile tile = myTileObject.GetComponent<Tile>();
			if (tile){
				tile.row = row;
				tile.col = col;
				tile.isEdge = isEdge;
				tile.myWall = myWall;
				tile.myWallIndex = myWallIndex;
				tile.myType = myType;
				tile.tilePosition = defaultTile.transform.position;
				tile.tileNeighbours = defaultTile.GetComponent<DefaultTile>().tileNeighbours;
				EditorUtility.SetDirty(tile);
			}
			
			myTileObject.transform.position = defaultTile.transform.position;
			myTileObject.transform.rotation = defaultTile.transform.rotation;
			myTileObject.transform.parent = transform;

		} else {
			defaultTile.SetActive (true);
			myTypeIndex = -1;
			myType = "";
		}
		
		EditorUtility.SetDirty (this);
	}
	#endif
}
