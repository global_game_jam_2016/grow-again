﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class Cube : MonoBehaviour {

	public GameObject defaultTile;
	public int cubeSize = 4;
	public GameObject[] tileHolderArray;
	public GameObject[] sides;
	Dictionary<Vector3, Tile> tilesDictionary;
	public Tile[] allTilesTemp;
	
	//[HideInInspector]
	//public List<Dictionary<Vector3, TileCreator>> sideMaps = new List<Dictionary<Vector3, TileCreator>>();
	
	public static Cube instance;
	
	void Awake () {
		instance = this;
		print (tileHolderArray.Length);
		SaveTilesInDictionary();
	}
	
	void Update () {
	
	}
	
	public Tile GetTile(int wall, int x, int y){
		//x %= cubeSize; // y is never over cube size
		//y %= cubeSize; // y is never over cube size
		Tile t = tileHolderArray[(x*cubeSize + y)+(wall*(cubeSize*cubeSize))].GetComponentInChildren<Tile>();
		return t;	
	}
	 
	public Tile FindTile(Vector3 position){

//		if (tilesDictionary.ContainsKey(position)){
//			return tilesDictionary[position];
//		} else {
//			print ("can't find key: " + position.ToString());
//			return null;
//		}
		
		for (int i = 0; i < allTilesTemp.Length; ++i) {
			if (allTilesTemp [i].transform.position == position) {
				return allTilesTemp [i];
			}	
		}
		return null;
	}
	
	
	void FindTileNeighbours(){
		foreach (GameObject t in tileHolderArray){
			Tile tile = t.GetComponentInChildren<Tile>();
			tile.FindNeighbours();	
		}
	}
	
	void SaveTilesInDictionary(){
		tilesDictionary = new Dictionary<Vector3, Tile>();
		
		Tile[] tiles = GameObject.FindObjectsOfType<Tile> ();
		foreach (Tile t in tiles){
			tilesDictionary.Add(t.transform.position, t);
		}
		
		allTilesTemp = tiles;
	}
	
#if UNITY_EDITOR
	public void BuildCube(){
		
		GetComponent<BoxCollider>().size = new Vector3(cubeSize, cubeSize, cubeSize);
		
		MeshRenderer tileRenderer = defaultTile.GetComponent<DefaultTile>().visMesh;
		
		if (tileRenderer){
		
			float tileW = tileRenderer.bounds.size.x;
			float tileH = tileRenderer.bounds.size.z;
			float tileY = tileRenderer.bounds.size.y;
			float cubeW = tileW * cubeSize;
			float cubeH = tileH * cubeSize;

			tileHolderArray = new GameObject[sides.Length * (cubeSize*cubeSize)];
			
			for (int s = 0; s < sides.Length; ++s){	
				for (int r = 0; r < cubeSize; ++r){
					for (int c = 0; c < cubeSize; ++c){
						Vector3 spawnPos = transform.position;
						spawnPos.x = transform.position.x - cubeW/2 + tileW/2 + tileW*c;
						spawnPos.z = transform.position.z + cubeH/2 - tileH/2 - tileH*r;
						
						// create an empty gameobject for holding tiles
						GameObject tileHolder = new GameObject("TileHolder");
						TileCreator tileCreator = tileHolder.AddComponent<TileCreator>();
						tileHolder.transform.parent = sides[s].transform;
						
						// instantiate a default tile prefab and put it under the tile holder
						GameObject t = PrefabUtility.InstantiatePrefab(defaultTile) as GameObject;
						t.transform.position = spawnPos;
						t.transform.parent = tileHolder.transform;
						
						int index = (r*cubeSize + c)+(s*(cubeSize*cubeSize));
						// add tile holder to array
						tileHolderArray[index] = tileHolder;
						
						// tile setup
						tileCreator.row = r;
						tileCreator.col = c;
						tileCreator.myWall = sides[s];
						tileCreator.myWallIndex = s;
						
						if (r == 0 || r == cubeSize-1 || c == 0 || c == cubeSize-1)
							tileCreator.isEdge = true;
						else {
							tileCreator.isEdge = false;
						}
							
						tileCreator.TileSetup(t);
						
						EditorUtility.SetDirty(tileCreator);
					}
				}
			}
			
			EditorUtility.SetDirty(this);
			PositionSides(cubeSize, tileW, tileY);
			FindTileNeighbours();
			print("cube spawned");
		}
	}

	
	public void PositionSides(float cubeSize, float tileSize, float tileY){
		
		for (int s = 0; s < sides.Length; ++s){
			Vector3 setPos = transform.position;
			Vector3 setRot = transform.rotation.eulerAngles;
			
			float offset = 0;//tileY/2;
			
			switch (s){
				case 0:
				setRot.z = 180;
				setPos.y = -cubeSize/2 + offset;
				break;
				
				case 1:
				setRot.x = -90;
				setPos.z = -cubeSize/2 + offset;
				break;
				
				case 2:
				setRot.z = 0;
				setPos.y = cubeSize/2 + -offset;
				break;
	
				case 3:
				setRot.x = 90;
				setPos.z = cubeSize/2 + -offset;
				break;
				
				case 4:
				setRot.z = 90;
				setPos.x = -cubeSize/2 + offset;
				break;
				
				case 5:
				setRot.z = -90;
				setPos.x = cubeSize/2 + -offset;
				break;
			}
			
			sides[s].transform.position = setPos;
			sides[s].transform.rotation = Quaternion.Euler(setRot);
		}
		
		print("sides positioned");
	}
	
	public void DestroyCube(){
		foreach (GameObject g in tileHolderArray){
			DestroyImmediate(g);
		}
	
		foreach (GameObject s in sides){
			s.transform.position = transform.position;
			s.transform.rotation = transform.rotation;
		}
		
		transform.position = new Vector3(0, 0, 0);
		print ("cube destroyed, wallTiles length: " + tileHolderArray.Length.ToString());
	}
#endif
}
