﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public abstract class Tile : MonoBehaviour {

	public int row;
	public int col;
	[HideInInspector]
	public bool isEdge = false;
	[HideInInspector]
	public GameObject myWall;
	[HideInInspector]
	public int myWallIndex;
	[HideInInspector]
	public string myType;
	[HideInInspector]
	public Vector3 tilePosition;
	public bool canMoveOnto = true;
	public bool canMoveOff = true;
	[HideInInspector]
	public Mover moverOnTile;
	[HideInInspector]
	public bool isRooted = false;
	[HideInInspector]
	public Root myRoot;
	[HideInInspector]
	public bool isRootEnd = false;
	
	public bool isDestructable = false;
	public bool doGroundRoot = false;
	
	public Tile[] tileNeighbours;
	[HideInInspector]
	public MeshRenderer mr;
	[HideInInspector]
	public Material mat;
	[HideInInspector]
	public Material defaultTileMat;
	Color matColor;
	Color highlightColor;
	[HideInInspector]
	public BoxCollider boxCollider;
	
	
	public enum TileType{
		blank,
		power,
		corrupt,
		corruptEnd,
		water,
		obstacle,
		dirt
	}
	
	public TileType tileType = TileType.blank;
	
	void Awake(){
		boxCollider = GetComponentInChildren<BoxCollider>();
		boxCollider.enabled = false;
		mr = GetComponentInChildren<MeshRenderer>();
		mat = mr.material;
		matColor = mat.color;
		highlightColor = matColor;
		highlightColor.r *= .75f;
		defaultTileMat = Resources.Load("TileMat") as Material;
		
		FindNeighbours();
	}
	
	public virtual void MoveOn(Mover.MoverType moverType){
		//print ("move onto new tile of type: " + this.GetType().Name);
	}
	
	public virtual void MoveOff(Mover.MoverType moverType){
		
	}
	
	public virtual void ConnectRoot(Root baseRoot){
		//GetComponent<MeshRenderer> ().material.color = Color.yellow;
	}
	
	public void FindNeighbours(){
		tileNeighbours = new Tile[4];
		Vector3[] neighbourPos = new Vector3[4];
		Vector3[] forwardPos = new Vector3[4];
		float gridSize = 1f;
		
		neighbourPos[0] = transform.position + (transform.forward * gridSize);
		neighbourPos[1] = transform.position + (-transform.forward * gridSize);
		neighbourPos[2] = transform.position + (transform.right * gridSize);
		neighbourPos[3] = transform.position + (-transform.right * gridSize);
		
		forwardPos[0] = transform.forward;
		forwardPos[1] = -transform.forward;
		forwardPos[2] = transform.right;
		forwardPos[3] = -transform.right;
		
		for (int i = 0, max = 4; i < max; ++i){
	
			tileNeighbours[i] = TileSearch(neighbourPos[i]);
			
			if (tileNeighbours[i] == null){
				GameObject temp = new GameObject();
				temp.transform.parent = transform;
				temp.transform.position = transform.position;
				temp.transform.rotation = transform.rotation;
				
				if (i == 0)
					temp.transform.Rotate (0, 0, 0);
				if (i == 1)
					temp.transform.Rotate (0, 180, 0);
				if (i == 2)
					temp.transform.Rotate (0, 90, 0);
				if (i == 3)
					temp.transform.Rotate (0, -90, 0);
					
				
				temp.transform.Rotate (90, 0, 0);
				temp.transform.position += temp.transform.forward * gridSize / 2;
				temp.transform.position += temp.transform.up * gridSize / 2;
				
				tileNeighbours[i] = TileSearch(temp.transform.position);
				
				DestroyImmediate(temp);
			}
		}
	}
	
	public void SetNeighbourColliders(bool set){
		Color color = (set)?highlightColor:matColor;
		
		for (int i = 0, max = tileNeighbours.Length; i < max; ++i){
			
			Tile t = tileNeighbours[i];
			// toggle the collider, keep on if the tile is rooted
			if (t){
				if (!t.isRooted && t.CheckCanMoveOnto()){
					t.boxCollider.enabled = set;
					t.SetHighlight(set);
				} else {
					t.SetHighlight(false);
				}
			}
		}
	}
	
	public void SetHighlight(bool set){
		mat.color = set?highlightColor:matColor;
	}
	
	public virtual Tile TileSearch (Vector3 position)
	{
		Tile[] tiles = GameObject.FindObjectsOfType<Tile> ();
		for (int i = 0; i < tiles.Length; ++i) {
			if (tiles [i].transform.position == position) {
				return tiles [i];
			}	
		}
		return null;
	}
	
	void OnMouseOver(){
		if (isRooted){
			if (Input.GetMouseButtonDown(0) && canMoveOff){
				RootManager.instance.SelectRoot(this);
			}
		}
	}
	
	void OnMouseEnter(){
		AttemptRootGrow();
	}
	
	public bool CheckCanMoveOnto(){
		bool _canMoveOnto = canMoveOnto;
		
		if (isDestructable && RootManager.instance.selectedRoot.isPowered){
			_canMoveOnto = true;
		}
		
		if (RootManager.instance.selectedRoot.isCorrupted
		    && (tileType == TileType.dirt ||
		    	tileType == TileType.power)
		    ){
			_canMoveOnto = false;
		}
		
		return _canMoveOnto;
	}
	
	public void AttemptRootGrow(){
		if (RootManager.instance.isRooting && RootManager.instance.selectedRoot.strength > 0){
			
			if (!isRooted && CheckCanMoveOnto()){	
				RootManager.instance.NewRoot(this, true);
			}
		}
	}
	
	public void ChangeMaterial(Material setMaterial){
		mr.material = setMaterial;
	}
	
	void WrapRoot(Tile _startTile, Tile _endTile){
//		Vector3 startPos = RootManager.instance.selectedRootTile.transform.position;
//		Vector3 endPos = transform.position;
//		Vector3 crossPos = Vector3.Cross(startPos, endPos); 
//		
//		Vector3 v = myWall.transform.right; // Choose this as you wish
//		Vector3 AB = endPos - startPos;
//		Vector3 C_prime = startPos + AB / 2;
//		Vector3 C = C_prime + Vector3.Normalize(Vector3.Cross(AB, v)) * .5f;
//		cornerPos = C;
//		
//		RootManager.instance.selectedRoot.AddVisSegment(startPos, cornerPos);
//		RootManager.instance.selectedRoot.AddVisSegment(cornerPos, endPos);
//		
//		GameObject debugSphere = Instantiate(Resources.Load("DebugSphere")) as GameObject;
//		debugSphere.transform.position = cornerPos;
//		
//		//Instantiate(RootManager.instance.wrapRoot, RootManager.instance.selectedRootTile.transform.position, transform.rotation);
	}
	
}
