﻿using UnityEngine;
using System.Collections;

public class RootTilePower : Tile {

	void Start () {
	
	}
	
	public override void ConnectRoot(Root baseRoot){
		baseRoot.ChangeState(Root.RootState.powered);
		RootManager.instance.GroundRoot(this);
	}
}
