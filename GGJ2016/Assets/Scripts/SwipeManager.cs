﻿using UnityEngine;
using System.Collections;

public class SwipeManager : MonoBehaviour {

	Vector2 firstPressPos;
	Vector2 secondPressPos;
	Vector2 currentSwipe;
	bool isSwiping = false;
	
	public enum CanSwipe{anywhere, onCollider};
	public CanSwipe canSwipe;
	public float minSwipeLegnth = 100f;
	public float threshold = 0.5f;
	
	void OnSwipeUp(){
		CameraControl.instance.Rotate(new Vector2(0, -1));
	}
	
	void OnSwipeDown(){
		CameraControl.instance.Rotate(new Vector2(0, 1));
	}
	
	void OnSwipeRight(){
		CameraControl.instance.Rotate(new Vector2(-1, 0));
	}
	
	void OnSwipeLeft(){
		CameraControl.instance.Rotate(new Vector2(1, 0));
	}
	
	void OnMouseOver(){
		if (canSwipe == CanSwipe.onCollider){
			CheckSwipe();
		}
	}
	
	void Update(){
		if (canSwipe == CanSwipe.anywhere){
			CheckSwipe();
		}
		
		SwipeControl();
	}
	
	void CheckSwipe(){
		if (Input.GetMouseButtonDown(0) && !isSwiping){
			firstPressPos = Input.mousePosition;
			isSwiping = true;
		}
	}
	
	void SwipeControl(){
		if (Input.GetMouseButtonUp(0) && isSwiping){
			
			//save ended touch 2d point
			secondPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y); 
			
			//create vector from the two points
			currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
			
			if (currentSwipe.magnitude > minSwipeLegnth){
				
				//normalize the 2d vector
				currentSwipe.Normalize();
				
				//swipe upwards
				if(currentSwipe.y > 0 && currentSwipe.x > -threshold && currentSwipe.x < threshold)
				{
					OnSwipeUp();
				}
				//swipe down
				if(currentSwipe.y < 0 && currentSwipe.x > -threshold && currentSwipe.x < threshold)
				{
					OnSwipeDown();
				}
				//swipe left
				if(currentSwipe.x < 0 && currentSwipe.y > -threshold && currentSwipe.y < threshold)
				{
					OnSwipeLeft();
				}
				//swipe right
				if(currentSwipe.x > 0 && currentSwipe.y > -threshold && currentSwipe.y < threshold)
				{
					OnSwipeRight();
				}
			}
			
			isSwiping = false;
		}
		
		
	}
}
