﻿using UnityEngine;
using System.Collections;

public class MovingObstacle : Mover {

	public enum MoveType{
		onPlayerMove,
		onTimer,
		mirrorPlayer,
		onRootMove
	}
	
	public MoveType moveType = MoveType.onPlayerMove;
	public Vector2[] moveSequence;
	int moveIndex = 0;
	public float moveTime = 1f;
	
	void Start () {
		base.Start();
		
		if (moveType == MoveType.onTimer)
			Invoke ("Go", moveTime);
	}
	
	public void Go(){
		Move (moveSequence[moveIndex]);
			
		if (moveType == MoveType.onTimer)	
			Invoke("Go", moveTime);
	}
	
	public override void OnLeaveTile ()
	{
		base.OnLeaveTile ();
		if (moveIndex < moveSequence.Length-1)
			moveIndex++;
		else
			moveIndex = 0;
	}
	
	public override void OnNewTile ()
	{
		base.OnNewTile ();
		currentTile.canMoveOnto = false;
		lastTile.canMoveOnto = true;
	}
	
	void Update () {
		base.Update();
	}
}
