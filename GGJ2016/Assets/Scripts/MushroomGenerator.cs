﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MushroomGenerator : MonoBehaviour
{

	public GameObject mushroom1;
	public GameObject mushroom2;

	public Vector3 dir;
	public Transform myTileTransform;
	public float addRotation = 0;
	// Use this for initialization
	void Start ()
	{
		StartCoroutine ("Grow");
	}

	IEnumerator Generator ()
	{
		GameObject go = Instantiate (mushroom1, new Vector3 (transform.position.x, 
		                                                     transform.position.y, 
		                                                     transform.position.z), Quaternion.identity) as GameObject;
		go.GetComponent<Renderer> ().enabled = false;

		go.transform.DOPunchScale (new Vector3 (0.05f, 0, 0.05f), 1f, 5, 0.5f);
		go.transform.SetParent (transform);
		go.transform.RotateAround (transform.position, dir, 90 + addRotation);
		go.GetComponent<Renderer> ().enabled = true;


		yield return new WaitForSeconds (0.1f);

		GameObject top = Instantiate (mushroom2, new Vector3 (transform.position.x, 
		                                                      transform.position.y + mushroom1.transform.localScale.y,// + mushroom2.transform.localScale.y / 2, 
		                                                      transform.position.z), Quaternion.identity) as GameObject;
		top.GetComponent<Renderer> ().enabled = false;

		top.transform.DOPunchScale (new Vector3 (0.05f, 0, 0.05f), 1f, 5, 0.5f);
		top.transform.SetParent (transform);
		top.transform.RotateAround (transform.position, dir, 90 + addRotation);
		top.GetComponent<Renderer> ().enabled = true;


	}

	public void Grow ()
	{
		StartCoroutine ("Generator");
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
