﻿using UnityEngine;
using System.Collections;

public abstract class Mover : MonoBehaviour {
	public enum MoverType{
		Player,
		MovingObstacle,
		PushBlock
	}
	
	public enum MoveStyle{
		freeMove,
		onePath
	}
	
	public MoveStyle moveStyle;
	public MoverType moverType;
	public GameObject visual;
	public Vector3 visualTargetPos;
	Quaternion visualTargetRotation;
	[HideInInspector]
	public bool canMove;
	[HideInInspector]
	public Vector2 lastInput;
	GameObject checkObj;
	public float gridSize = 1.0f;
	[HideInInspector]
	public Tile currentTile;
	[HideInInspector]
	public Tile lastTile;
	[HideInInspector]
	public Vector2 moveInput;
	public bool isPushable = false;
	public bool doRotateVisual = true;
	[HideInInspector]
	public Tile targetTile;
	
	public virtual void Start(){
		lastInput = new Vector2 (0, 1);
		
		visualTargetPos = transform.position;
		visualTargetRotation = transform.rotation;
		
		transform.parent = null;
		visual.transform.parent = null;
		
		currentTile = FindMyTile (); // this must be after setting parent to null
		lastTile = currentTile;
		currentTile.canMoveOnto = false;
		currentTile.moverOnTile = this;
		
		//visual = Instantiate (visual, visualTargetPos, visualTargetRotation) as GameObject;
	}
	
	public virtual void Update(){
		VisualFollow ();
	}
	
	public virtual bool Move (Vector2 input)
	{
		moveInput = input; // store move input for override functions
		
		// if moving in a new direction, change the rotation
		if (lastInput != input) {
			float angleChange = 0;
			if (input.x != 0) {
				if (lastInput.y == 0) {
					angleChange = 180;
				} else {
					angleChange = 90 * Mathf.Sign (lastInput.y) * input.x;
				}
			}
			
			if (input.y != 0) {
				if (lastInput.x == 0) {
					angleChange = 180;
				} else {
					angleChange = -90 * Mathf.Sign (lastInput.x) * input.y;
				}
			}
			transform.Rotate (0, angleChange, 0);
		}
		
		// find the target Tile, check for constraints, then move
		canMove = true;
		bool doRotate = false;
		Vector3 checkTilePos = transform.position + transform.forward * gridSize;
		targetTile = TileSearch (checkTilePos);
		
		if (!targetTile) {
			// rotate around the cube with a temporary obj and check for a tile
			checkObj = new GameObject ("TEMPORARY Check Object");
			checkObj.transform.position = transform.position;
			checkObj.transform.rotation = transform.rotation;
			checkObj.transform.position += checkObj.transform.forward * gridSize;
			
			RotateAroundCube (checkObj);
			
			targetTile = TileSearch (checkObj.transform.position);
			if (targetTile) {
				doRotate = true;
			} else {
				canMove = false;
				print ("Mover can't find tile. Something went wrong...");
			}
		} 
		
		if (targetTile) {
			if (targetTile.canMoveOnto == false)
				canMove = false;
				
			// push objects 	
			if (targetTile.moverOnTile != null){
				if (targetTile.moverOnTile.isPushable){
					canMove = false;
					
					bool canPush = targetTile.moverOnTile.Move(input);
					
					if (canPush)
						canMove = true;
				}
			}
			
			if (moverType == MoverType.Player){
			
				if (moveStyle == MoveStyle.onePath){
					if (targetTile.myType == "Dirt"){
						if (targetTile.GetComponent<Dirt>().hasBloomed){
							canMove = false;
						}
					}
				}
				
//				if (checkTile.myType == "PlayerStart"){
//					if (LevelManager.instance.CheckWin()){
//						canMove = false;
//						GrowTree.instance.StartGrowing();
//						print ("grow tree");
//					}
//				}
			}
			
			
		}
		
		if (canMove) {
			if (doRotate) {
				transform.position += transform.forward*gridSize;
				RotateAroundCube (this.gameObject);
				OnRotate();
			} else {	
				transform.position = targetTile.transform.position;
			}
			
			transform.position = targetTile.transform.position;
				
			OnLeaveTile();
			
			currentTile.MoveOff(moverType);
			
			FindMyTile (); 
			
			currentTile.MoveOn(moverType);
			
			SetVisualTarget (); 
			
			OnNewTile();
		}
		
		// save the last input
		lastInput = input;
		
		if (checkObj)
			Destroy (checkObj);
			
		//string check = (transform.position == checkTile.transform.position)?"GOOD! ":"BAD! ";
		//print (check + "player: " +  transform.position.ToString() + " tile: " + checkTile.transform.position.ToString());
		
		return canMove;
	}
	
	public virtual void OnRotate(){
	
	}
	
	public virtual void OnNewTile(){
		currentTile.moverOnTile = this;
	}
	
	public virtual void OnLeaveTile(){
		if (currentTile.moverOnTile == this)
			currentTile.moverOnTile = null;
	}
	
	void VisualFollow ()
	{
		float speed = 5.0f;
		float pivotSpeed = 9.0f;
		visual.transform.position = Vector3.Lerp (visual.transform.position, visualTargetPos, speed * Time.deltaTime);
		
		visual.transform.rotation = Quaternion.Slerp (visual.transform.rotation, visualTargetRotation, pivotSpeed * Time.deltaTime);
	}
	
	void SetVisualTarget ()
	{
		visualTargetPos = transform.position;
		visualTargetRotation = transform.rotation;
	}
	
	public virtual Tile FindMyTile ()
	{
		// find the new tile
		Tile newTile = TileSearch (transform.position);
		if (newTile) {
			lastTile = currentTile;
			currentTile = newTile;
		} else {
			currentTile = null;
			print ("can't find player's current tile");
		}
		
		transform.position = currentTile.transform.position;
		
		return currentTile;
	}
	
	public virtual Tile TileSearch (Vector3 position)
	{		
		return Cube.instance.FindTile(position);
	}
	
	public void RotateAroundCube (GameObject go)
	{
		go.transform.Rotate (90, 0, 0);

		// set new position 
		go.transform.position += go.transform.forward * gridSize / 2;
		go.transform.position += -go.transform.up * gridSize / 2;
	}
}
