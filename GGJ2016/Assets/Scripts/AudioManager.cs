﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {

	public AudioClip[] audioList = new AudioClip[0];

	private static Dictionary<string, AudioSource> audioSourceMap = new Dictionary<string, AudioSource>();

	// Use this for initialization
	void Start () {
		foreach (AudioClip audio in audioList) {
			AudioSource audioSource = this.gameObject.AddComponent<AudioSource>();
			audioSource.clip = audio;
			audioSourceMap.Add(audio.name, audioSource);
		}
	}

	public static void playOneShot(string clipName) {
		AudioSource audioSource = audioSourceMap[clipName];
		audioSource.loop = false;
		audioSource.Play();
	}

	public static void playLoop(string clipName) {
		AudioSource audioSource = audioSourceMap[clipName];
		audioSource.loop = true;
		audioSource.Play();
	}

	public static void stop(string clipName) {
		audioSourceMap[clipName].Stop();
	}

	public static void stopAll() {
		foreach (var item in audioSourceMap) {
			item.Value.Stop();
		}
	}
}
