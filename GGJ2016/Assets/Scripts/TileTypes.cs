﻿using UnityEngine;
using System.Collections;

public static class TileTypes {

	public static string[] tileTypes = new string[]
	{
		"None", 
		"Dirt", 
		"PlayerStart", 
		"Rock", 
		"ChantTile", 
		"MovingObstacleTile", 
		"PushBlockTile", 
		"Spawner",
		"RootStart",
		"RootWater",
		"RootFlower",
		"RootRock",
		"RootPower",
		"RootCorruption",
		"RootCorruptionEnd",
		"RootGoalDoor"
	};

}
