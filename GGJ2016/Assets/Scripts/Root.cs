using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Root : MonoBehaviour {

	[HideInInspector]
	public Tile startTile; 
	public Tile firstTile;
	[HideInInspector]
	public GameObject NathanRootPrefab;
	[HideInInspector]
	public List<Tile> rootTiles;
	[HideInInspector]
	public List<LineRenderer> rootLines = new List<LineRenderer>();
	[HideInInspector]
	public List<GameObject> rootGameObjects = new List<GameObject>();
	[HideInInspector]
	public bool isAlive = false;
	GameObject rootPrefab;
	public Tile rootEnd{
		get{
			return rootTiles[rootTiles.Count-1];
		}
	}
	
	public Tile rootStart{
		get{
			return rootTiles[0];
		}
	}
	
	[HideInInspector]
	public bool hasSetup = false;
	[HideInInspector]
	public int strength;
	
	public int maxStrength = 10;

	TextMesh debugText;
	//[HideInInspector]
	public bool isPowered = false;
	//[HideInInspector]
	public bool isCorrupted = false;
	
	[HideInInspector]
	public List<RootTileCorruption> corruptedTiles;
	
	Color idleCol;
	Color hightlightCol;
	Color rootColNeutral = Color.green;
	Color rootColPower = Color.red;
	Color rootColCorrupted = Color.magenta;
	
	[HideInInspector]
	public bool isHighlighted = false;
	
	public enum RootState{
		neutral,
		powered,
		corrupted
	}
	
	[HideInInspector]
	public RootState rootState = RootState.neutral;
	
	public bool doNathanRoot = true;
	
	void Start () {
		if (!hasSetup){
			startTile = GetComponent<Tile>();
			GrowTree tree = GetComponent<GrowTree>();
			if (tree){
				tree.StartGrowing();
			}
			
			RootSetup(startTile, firstTile, this, false);
			firstTile.GetComponent<BoxCollider>().enabled = true; // enable selection of first tile
			
			SetNeutral();
			SetHighlight(false);
			
			LevelManager.instance.SetStep(strength);
		}

	}
	
	public void ChangeState(RootState state){
		rootState = state;
		
		switch (state){
			case RootState.corrupted:
				SetCorrupted(true);
			break;
			
			case RootState.neutral:
				SetNeutral();
			break;
			
			case RootState.powered:
				SetPower(true);
			break;
		}
		
		SetHighlight(isHighlighted);
	}
	
	public void SetNeutral(){
		SetPower(false);
		SetCorrupted(false);
		SetRootColour(rootColNeutral); 
	}
	
	public void SetPower(bool set){
		isPowered = set;

		if (set) {
			SetRootColour(rootColPower); 
		}
	}
	
	public void SetCorrupted(bool set){
		isCorrupted = set;
		
		if (set){
			SetRootColour(rootColCorrupted); 
		} else {
			foreach (RootTileCorruption c in corruptedTiles){
				c.SetCorruption(false);
			}
		}
	}
	
	public void SetRootColour(Color c){
		idleCol = c;
		hightlightCol = c;
		hightlightCol.a = .75f;
	}
	
	public void SetHighlight(bool set){
		Color setColor = set?hightlightCol:idleCol;
		foreach (LineRenderer r in rootLines){
			r.SetColors(setColor, setColor);
		}	
	}
	
	public void RootSetup(Tile _startTile, Tile _firstSegment, Root _rootParent, bool doUseStrength){
		startTile = _startTile;
		firstTile = _firstSegment;
		firstTile.isRootEnd = true;
		
		strength = maxStrength;
		
		rootPrefab = Resources.Load("Root") as GameObject;
		
		debugText = GetComponentInChildren<TextMesh>();
		
		if (rootTiles == null){
			rootTiles = new List<Tile>();
		}
		
		AddSegment(startTile, false);
		AddSegment(firstTile, doUseStrength);
		
		SetHighlight(true);
		
		hasSetup = true;
	}
	
	public void AddVisSegment(Vector3 startPos, Vector3 endPos){
		GameObject root = Instantiate(rootPrefab) as GameObject;
		root.transform.parent = gameObject.transform;
		LineRenderer r = root.GetComponent<LineRenderer>();
		
		r.SetPosition(0, startPos);
		r.SetPosition(1, endPos);
		
		rootLines.Add(r);
		
		AdjustRootSize(r);
	}
	
	
	public void AddSegmentBranch(Tile newTile, Tile prevTile, bool doUseStrength){
		AddSegment(newTile, doUseStrength);
		rootLines[rootLines.Count-1].SetPosition(0, prevTile.transform.position);
	}
	
	public void AddSegment(Tile tile, bool doUseStrength){
		if (doUseStrength){
			ChangeStrength(-1);
		}
		
		tile.isRooted = true;
		tile.myRoot = this;
		
//		if (RootManager.instance.selectedRootTile.isRootEnd){
//			RootManager.instance.selectedRootTile.isRootEnd = false;
//		}
		
		rootTiles.Add(tile);

		GameObject root = Instantiate(rootPrefab) as GameObject;
		root.transform.parent = gameObject.transform;
		LineRenderer r = root.GetComponent<LineRenderer>();

		rootLines.Add(r);
		Transform startTile = null;
		if (rootTiles.Count > 1){
			startTile = rootTiles[rootTiles.Count-2].transform;
		} else {
			startTile = rootTiles[rootTiles.Count-1].transform;
		}
		
		Transform endTile = rootTiles [rootTiles.Count - 1].transform;
		Vector3 posStart = startTile.transform.position;
		Vector3 posEnd = endTile.transform.position;
		r.SetPosition(0, posStart);
		r.SetPosition(1, posEnd);

		if (doNathanRoot){
			NathanRootPrefab = Resources.Load("NathanRootPrefab") as GameObject;
			GameObject Root = Instantiate(NathanRootPrefab, posStart, Quaternion.identity) as GameObject;
			RootSegment seg = Root.GetComponentInChildren<RootSegment>();
			seg.Grow(startTile,endTile);
			rootGameObjects.Add (Root);
			//float sMod = scaleMod ();
			//Root.transform.localScale = new Vector3 (sMod, sMod, 1);
		}
		
		debugText.text = strength.ToString();
		
		AdjustRootSize(r);
	}
	
	public void ChangeStrength(int change){
		strength += change;
		debugText.text = strength.ToString();
		LevelManager.instance.SetStep(strength);
	}
	
	public float scaleMod()
	{
		print ("Strength: " + strength.ToString () + " MAX STRENGTH: " + maxStrength.ToString ());
		float minWidth = .1f;
		float maxWidth = 1f;
		return Mathf.Lerp (minWidth, maxWidth, (float)strength / (float)maxStrength);
	}
	
	public void AdjustRootSize(LineRenderer r){
		float minWidth = .1f;
		float maxWidth = .3f;
		float increment = (maxWidth-minWidth) / maxStrength;
		float startWidth = maxWidth - (increment*(maxStrength-strength));
		float endWidth = startWidth - increment;
		r.SetWidth(startWidth, endWidth);
	}
	
	void Update () {
		
	}
}
