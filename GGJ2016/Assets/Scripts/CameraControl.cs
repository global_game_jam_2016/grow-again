﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

	public enum RotateStyle{
		overEdge,
		noRotate
	}
	
	public RotateStyle rotateStyle;
	
	public static CameraControl instance;
	[HideInInspector]
	public Quaternion targetRotation = Quaternion.identity;
	float swayTime = 0;
	float swayPeriod = 1f;
	float swayAngle = 3f;
	public bool doSway = false;
	
	void Start () {
		instance = this;
		targetRotation = transform.rotation;
	}
	
	public void Rotate(Vector2 direction){
		if (rotateStyle == RotateStyle.overEdge){
			Vector3 newRot = Vector3.zero;
			
			if (direction.x > 0)
				newRot.y = -90;
			if (direction.x < 0)
				newRot.y = 90;
			if (direction.y > 0)
				newRot.x = 90;
			if (direction.y < 0)
				newRot.x = -90;
				
			Quaternion newRotation = targetRotation;
			newRotation *= Quaternion.Euler(newRot);
			targetRotation = newRotation;
		}

	}
	
	void Update () {

		if (doSway){
			swayTime += Time.deltaTime;
			float phase = Mathf.Sin(swayTime / swayPeriod);
			Vector3 targetAngles = targetRotation.eulerAngles;
			targetAngles.x += (phase * swayAngle) * Mathf.PerlinNoise(1f * Time.deltaTime, 1f * Time.deltaTime);
			targetAngles.y += (phase * swayAngle) * Mathf.PerlinNoise(1f * Time.deltaTime, 1f * Time.deltaTime);
			Quaternion newRotation = Quaternion.Euler(targetAngles); 
			transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 5f);
		} else {
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 5f);
		}
		
			
	}
	
	void OnMouseOver(){
		print ("mouse in sky");
	}
}
