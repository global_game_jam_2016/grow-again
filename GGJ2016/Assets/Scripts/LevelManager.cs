﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

	public static LevelManager instance;
	
	public int dirtGrown = 0;
	public int totalDirt;
	public int chantTilesActive = 0;
	public int totalChant;
	
	public bool hasWon = false;
	public bool hasGrownAll = false;
	public bool hasChantedAll = false;
	
	public Text stepsText;
	public Text dirtTilesText;
	public Text chantTilesText;
	public Text nextLevelText;
	public Image dirtImage;
	public Image chantImage;
	
	int steps = 0;
	bool canContinue = false;
	
	void Awake () {
		if (instance != this)
			Destroy(instance);
			
		instance = this;
		
		Object[] dirtTiles = GameObject.FindObjectsOfType (typeof(Dirt));
		totalDirt = dirtTiles.Length;
		
		Object[] chantTiles = GameObject.FindObjectsOfType (typeof(ChantTile));
		totalChant = chantTiles.Length;
		
		if (totalChant == 0){
			chantImage.enabled = false;
			chantTilesText.enabled = false;
		}
		
		print ("total dirt: " + dirtTiles.Length.ToString ());
		
		stepsText.text = steps.ToString();
		
		UpdateDirtText();
		
		UpdateChantText();
		
		if (dirtTiles.Length == 0){
			hasGrownAll = true;
		}
		
		if (chantTiles.Length == 0){
			hasChantedAll = true;
		}
			
		nextLevelText.enabled = false;
		
	}
	
	public bool CheckWin(){
		if (hasGrownAll && hasChantedAll){
			Invoke ("EnableContinue", 2f);
			hasWon = true;
		}
		return hasWon;
	}
	
	public void RootWin(){
		hasWon = true;
		Invoke ("EnableContinue", .5f);
	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.R)){
			Application.LoadLevel(Application.loadedLevel);
		}
		
		if (canContinue){
			if (Input.anyKeyDown){
                Fader.instance.BlackFadeIn(2f);
				if (Application.loadedLevel < Application.levelCount-1)
					Application.LoadLevel(Application.loadedLevel+1);
				else
					Application.LoadLevel(0);
			}
		}
	}
	
	public void Step(){
		steps++;
		stepsText.text = steps.ToString();
	}
	
	void EnableContinue(){
		nextLevelText.enabled = true;
		canContinue = true;
	}
	
	public void SetStep(int num){
		stepsText.text = num.ToString();
	}
	
	public void DirtCount (int change)
	{
		dirtGrown += change;
		if (dirtGrown == totalDirt){
			hasGrownAll = true;
			print ("all dirt tiles active");
		}
		
		UpdateDirtText();
	}
	
	public void ChantCount (int change)
	{
		chantTilesActive += change;
		if (chantTilesActive == totalChant){
			hasChantedAll = true;
			print ("all chant tiles active");
		}
		
		UpdateChantText();
	}
	
	void UpdateDirtText(){
		dirtTilesText.text = dirtGrown.ToString() + " / " + totalDirt.ToString();
	}
	
	void UpdateChantText(){
		chantTilesText.text = chantTilesActive.ToString() + " / " + totalChant.ToString();
	}
}
