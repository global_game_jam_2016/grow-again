﻿using UnityEngine;
using System.Collections;

public class PlayerSpawn : Tile {

	void Start () {
		GameObject player = Instantiate (Resources.Load ("Player")) as GameObject;
		Vector3 spawnPos = transform.parent.position;
		player.transform.position = spawnPos;
		player.transform.rotation = transform.rotation;
	}

}
