﻿using UnityEngine;
using System.Collections;

public class CameraSwipeControl : MonoBehaviour {

	Vector2 firstPressPos;
	Vector2 secondPressPos;
	Vector2 currentSwipe;
	public float minSwipeLegnth = 200f;
	bool isSwiping = false;
	Vector2 lastMousePosition;
	public GameObject cameraHolder;
	float lastXDir = 1;
	float lastYDir = 1;
	public enum ControlStyle{
		drag,
		swipe
	}
	public ControlStyle controlStyle = ControlStyle.swipe;
	
	void Start () {
	
	}
	
	void Update () {
	
		if (controlStyle == ControlStyle.drag && isSwiping){ 
			CameraDrag();
		}
		
		if (controlStyle == ControlStyle.swipe && isSwiping){
			SwipeControl();
		}
	}
	
	void OnMouseOver(){
		if (Input.GetMouseButtonDown(0) && !isSwiping){
			print ("start camera drag");
			firstPressPos = Input.mousePosition;
			lastMousePosition = Input.mousePosition;
			isSwiping = true;
		}
	}
	
	void SwipeControl(){
		if (Input.GetMouseButtonUp(0)){
			
			//save ended touch 2d point
			secondPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y); 
			
			//create vector from the two points
			currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
			
			if (currentSwipe.magnitude > minSwipeLegnth){
				
				//normalize the 2d vector
				currentSwipe.Normalize();
				
				float threshold = 0.5f;
				
				//swipe upwards
				if(currentSwipe.y > 0 && currentSwipe.x > -threshold && currentSwipe.x < threshold)
				{
					Debug.Log("up swipe");
					CameraControl.instance.Rotate(new Vector2(0, -1));
				}
				//swipe down
				if(currentSwipe.y < 0 && currentSwipe.x > -threshold && currentSwipe.x < threshold)
				{
					Debug.Log("down swipe");
					CameraControl.instance.Rotate(new Vector2(0, 1));
				}
				//swipe left
				if(currentSwipe.x < 0 && currentSwipe.y > -threshold && currentSwipe.y < threshold)
				{
					Debug.Log("left swipe");
					CameraControl.instance.Rotate(new Vector2(1, 0));
				}
				//swipe right
				if(currentSwipe.x > 0 && currentSwipe.y > -threshold && currentSwipe.y < threshold)
				{
					Debug.Log("right swipe");
					CameraControl.instance.Rotate(new Vector2(-1, 0));
				}
			}
			
			isSwiping = false;
		}
		
		
	}
	
	void CameraDrag(){
		if ((Vector2)Input.mousePosition != lastMousePosition){
			
			float diffX = firstPressPos.x - Input.mousePosition.x;
			float diffY = firstPressPos.y - Input.mousePosition.y;
			float xDir = Mathf.Sign(diffX)*1;
			float yDir = Mathf.Sign(diffY)*1;
			
			
			if (xDir != lastXDir || yDir != lastYDir){
				firstPressPos = Input.mousePosition;
			}				
			
			//print ("diffX " + diffX.ToString() + " diffY " + diffY.ToString());
			
			float unit = Screen.width / (360*2);
			Vector3 newCameraRot = cameraHolder.transform.rotation.eulerAngles;
			newCameraRot.x += -diffY * .01f;
			newCameraRot.y += diffX * .01f;
			cameraHolder.transform.rotation = Quaternion.Euler(newCameraRot);
			//cameraHolder.transform.rotation *= Quaternion.AngleAxis(diffY, Vector3.forward);
			
			lastXDir = xDir;
			lastYDir = yDir;
		}
		
		lastMousePosition = Input.mousePosition;
		
		if (Input.GetMouseButtonUp(0)){
			isSwiping = false;
		}
	}
}
