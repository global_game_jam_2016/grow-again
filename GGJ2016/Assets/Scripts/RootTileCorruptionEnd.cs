﻿using UnityEngine;
using System.Collections;

public class RootTileCorruptionEnd : Tile {
	
	void Start(){
		
	}
	
	public override void ConnectRoot(Root root){
		if (RootManager.instance.selectedRoot.isCorrupted){
			root.ChangeState(Root.RootState.neutral);
			ChangeMaterial(defaultTileMat);
		}
	}
}
