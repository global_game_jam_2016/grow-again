﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Dirt : Tile
{
	Color startCol;
	public bool hasBloomed = false;
	
	public GameObject tree;
	public GameObject mushroom;
	
	bool isTree = false;
	public GameObject[] flowerPrefabs;
	GameObject[] myFlowers;
	
	public ParticleSystem sparkle;
	
	void Start ()
	{
		startCol = GetComponent<MeshRenderer> ().material.color;
	}
	
	public override void MoveOn (Mover.MoverType moverType)
	{
		print ("move onto dirt tile");
		
		if (!LevelManager.instance.hasGrownAll && (moverType == Mover.MoverType.Player || moverType == Mover.MoverType.MovingObstacle)){
			hasBloomed = !hasBloomed;
			if (hasBloomed)
				//ChangeColour (Color.green);
				GrowFlowers();
			else{
				WitherFlowers();
				//WitherVictor ();
				//ChangeColour (startCol);
			}
		} else {
			if (LevelManager.instance.hasGrownAll){
				print ("stepped on dirt, but already grew them all! SAfhASKJGHAKSJGH!!");
			}
		}
	}
	
	public override void MoveOff(Mover.MoverType moverType)
	{	
		if (moverType == Mover.MoverType.Player || moverType == Mover.MoverType.MovingObstacle){
			if (hasBloomed) {
				//GrowFlowers();
				//GrowVictor (transform.parent);
			}
		}
	}
	
	public void GrowFlowers(){
		
		LevelManager.instance.DirtCount(1);
		
		sparkle.Play();
		
		int flowersToGrow = Random.Range(5, 7);
		myFlowers = new GameObject[flowersToGrow];
		
		for (int i = 0; i < flowersToGrow; ++i){
			GameObject prefab = flowerPrefabs[Random.Range(0, flowerPrefabs.Length)];
			GameObject f = Instantiate(prefab, transform.position, transform.rotation) as GameObject; 
			Flower flower = f.GetComponent<Flower>();
			
			float scale = Random.Range(3f, 5f);
			flower.targetScale = new Vector3(scale, scale, scale);
			//flower.transform.localScale = new Vector3(scale, scale, scale);
			
			float tileSize = 1f;
			float spawnRange = tileSize * .75f;

			Vector3 spawnPos = transform.TransformPoint(Random.Range(-spawnRange/2, spawnRange/2), 
			                                            Random.Range(-spawnRange/2, spawnRange/2),
			                                            Random.Range(-spawnRange/2, spawnRange/2));
			flower.transform.position = spawnPos;
			
			myFlowers[i] = flower.gameObject;
		}
	}
	
	void WitherFlowers(){
	
		LevelManager.instance.DirtCount(-1);
		
		foreach (GameObject f in myFlowers){
			f.GetComponent<Flower>().StartWither();
		}
	}
	
	void GrowVictor (Transform tileTransform)
	{
		LevelManager.instance.DirtCount(1);
		
		Vector3 setDir = tileTransform.transform.forward;
		float addRotation = 0;
		if (tileTransform.transform.up == -Cube.instance.transform.forward){
			setDir = -tileTransform.transform.right;
		}
		if ( tileTransform.transform.up == Cube.instance.transform.forward){
			setDir = tileTransform.transform.right;
		}
		if ( tileTransform.transform.up == Cube.instance.transform.up){ // up
			setDir = tileTransform.transform.forward ;//
			Quaternion newRot = Quaternion.Euler(setDir);
			Vector3 vectorRot = new Vector3(0, 90, 0);
			setDir = newRot * vectorRot;
		}
		if ( tileTransform.transform.up == -Cube.instance.transform.up){ // down
			addRotation = 90;
			setDir = -tileTransform.transform.right;
			Quaternion newRot = Quaternion.Euler(setDir);
			Vector3 vectorRot = new Vector3(90, 0, 0);
			setDir = (newRot * vectorRot);
		}
		if ( tileTransform.transform.up == Cube.instance.transform.right){
			setDir = -tileTransform.transform.forward;
		}
		if ( tileTransform.transform.up == -Cube.instance.transform.right){
			setDir = tileTransform.transform.forward;
		}
		
		int r = Random.Range (1, 3);
		if (r == 1) {
			GameObject go = Instantiate (tree, transform.position, Quaternion.identity) as GameObject;

			
			go.GetComponent<TreeGenerator> ().dir = setDir;
			go.GetComponent<TreeGenerator> ().addRotation = addRotation;
			go.GetComponent<TreeGenerator> ().myTileTransform = tileTransform;
			go.GetComponent<TreeGenerator> ().Grow ();
			go.transform.SetParent (transform);
		} else {
			GameObject go = Instantiate (mushroom, transform.position, Quaternion.identity) as GameObject;
			go.GetComponent<MushroomGenerator> ().dir = setDir;
			go.GetComponent<MushroomGenerator> ().addRotation = addRotation;
			go.GetComponent<MushroomGenerator> ().myTileTransform = tileTransform;
			go.GetComponent<MushroomGenerator> ().Grow ();
			go.transform.SetParent (transform);
		}
		
	}
	
	void WitherVictor ()
	{
		LevelManager.instance.DirtCount(-1);
		
		foreach (Renderer r in transform.GetChild (0).gameObject.GetComponentsInChildren<Renderer>()) {
			r.material.DOFade (0, 0.5f).OnComplete (DestroyGrowth);
		}
	}
	
	void DestroyGrowth ()
	{
		Destroy (transform.GetChild (0).gameObject);
		
	}
	
	public void ChangeColour (Color col)
	{
		MeshRenderer mr = GetComponent<MeshRenderer> ();
		if (mr)
			mr.material.color = col;
	}
}
