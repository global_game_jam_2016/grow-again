﻿using UnityEngine;
using System.Collections;

public class RootTileCorruption : Tile {
	
	public GameObject corruptionVis;
	
	void Start(){

	}
	
	public override void ConnectRoot(Root root){
		root.ChangeState(Root.RootState.corrupted);
		root.corruptedTiles.Add(this);
	}
	
	public void SetCorruption(bool set){
		corruptionVis.SetActive(set);
		
		ChangeMaterial(set?mat:defaultTileMat);
		
		if (!set){
			canMoveOff = true;
		}
	}
}
