﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    public static MainMenu instance;
    bool isPress = false;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        Fader.instance.BlackFadeIn(2f);
        Fader.instance.WhiteFadeIn(0.001f);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0)) {
            isPress = true;
        }
        if (isPress)
        {
            Fader.instance.WhiteFadeOut(2f);
            Application.LoadLevel(Application.loadedLevel + 1);
        }
    }
}
