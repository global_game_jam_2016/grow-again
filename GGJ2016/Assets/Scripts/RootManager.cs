﻿using UnityEngine;
using System.Collections;

public class RootManager : MonoBehaviour {

	public Root selectedRoot = null;
	public static RootManager instance;
	public bool isRooting = false;
	public Tile selectedRootTile;
	public GameObject normalRoot;
	public GameObject wrapRoot;
	public Material defaultTileMat;
	MovingObstacle[] movingObstacles;
	public bool canBranch = true;
	
	void Awake () {
		instance = this;
		movingObstacles = GameObject.FindObjectsOfType<MovingObstacle>();
	}

	void Update () {
		if (Input.GetMouseButtonUp(0) && isRooting){
			DeselectRoot();
		}
	}
	
	public void DeselectRoot(){
		isRooting = false;
		selectedRootTile.SetNeighbourColliders(false);
		selectedRoot.SetHighlight(false);
		//selectedRoot = null;
	}
	
	public void SelectRoot(Tile tile){
		selectedRoot = tile.myRoot;
		selectedRoot.SetHighlight(true);
		isRooting = true;
		selectedRootTile = tile;
		tile.SetNeighbourColliders(true);
	}
	
	public void NewRoot(Tile tile, bool doUseStrength){
		// if the end of the root, add a segment, otherwise add a new root branch
		if (selectedRootTile.isRootEnd){
			selectedRoot.AddSegmentBranch(tile, selectedRootTile, doUseStrength);
		} else {
			if (canBranch){
				selectedRoot.AddSegmentBranch(tile, selectedRootTile, doUseStrength);
			} else {
				print ("can't branch");
				return;
			}
		}
		
		if (selectedRootTile.isRootEnd){
			selectedRootTile.isRootEnd = false;
			tile.isRootEnd = true;
		} else {
			tile.isRootEnd = true;
		}
		
		tile.isRooted = true;
		tile.myRoot = selectedRoot;
		
		if (tile.myWallIndex != selectedRootTile.myWallIndex){
			//tile.WrapRoot(tile, selectedRootTile);
		}
		
		selectedRootTile.SetNeighbourColliders(false);
		tile.SetNeighbourColliders(true);
		
		selectedRootTile = tile;
		selectedRoot.SetHighlight(true);
		
		tile.ConnectRoot(selectedRoot);
		
		
		if (!tile.canMoveOff){
			DeselectRoot();
		}
		
		if (tile.doGroundRoot){
			GroundRoot(tile);
		}
		
		tile.canMoveOnto = false;
		MoveObstacles();
	}
	
	public void GroundRoot(Tile tile){
		tile.canMoveOff = false;
		if (isRooting){
			DeselectRoot();
		}	
	}
	
	void MoveObstacles(){
		
		for (int i = 0; i < movingObstacles.Length; ++i) {
			
			switch (movingObstacles[i].moveType){
			case MovingObstacle.MoveType.onRootMove: movingObstacles[i].Go(); break;
			}
			
		}
	}
}
