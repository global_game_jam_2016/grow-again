﻿using UnityEngine;
using System.Collections;

public class Flower : MonoBehaviour {

	float swayTime = 0;
	float swayPeriod = .5f;
	float swayAngle = .1f;
	public MeshRenderer[] pedals;
	public bool doRandomColor = false;
	public Gradient colorOptions;
	public Vector3 targetScale;
	float growSpeed = 2f;
	float growSpeedVariation = .25f;
	bool doGrow = false;
	[HideInInspector]
	public bool isDead = false;
	
	void Start () {
		swayTime = Random.Range(0.0f, 1.5f);
		if (doRandomColor){
			foreach (MeshRenderer p in pedals){
				p.material.color = colorOptions.colorKeys[Random.Range(0, colorOptions.colorKeys.Length)].color;
			}
		}
		transform.localScale = Vector3.zero;
		growSpeed = Random.Range(growSpeed - (growSpeed * growSpeedVariation), growSpeed - (growSpeed * growSpeedVariation));
		Invoke("StartGrow", Random.Range(0, .5f));
	}
	
	void Update () {
	
		if (!isDead){
			if (doGrow)
				Grow ();
				
			Sway ();
		} else {
			Wither ();
		}
	}
	
	void StartGrow(){
		doGrow = true;
	}
	
	void Grow(){
		if (transform.localScale != targetScale){
			transform.localScale = Vector3.Lerp(transform.localScale, targetScale, growSpeed * Time.deltaTime);
		}
	}
	
	public void StartWither(){
		isDead = true;
	}
	
	void Wither(){
		transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, growSpeed*3 * Time.deltaTime);
		if (transform.localScale == Vector3.zero){
			Destroy(this.gameObject);
			print ("flower wither");
		}
		
	}
	
	void Sway(){
		swayTime += Time.deltaTime;
		float phase = Mathf.Sin(swayTime / swayPeriod);
		
		Vector3 targetAngles = transform.rotation.eulerAngles;
		targetAngles.x += (phase * swayAngle) * Mathf.PerlinNoise(1f * Time.deltaTime, 1f * Time.deltaTime);
		targetAngles.y += (phase * swayAngle) * Mathf.PerlinNoise(1f * Time.deltaTime, 1f * Time.deltaTime);
		targetAngles.z += (phase * swayAngle) * Mathf.PerlinNoise(1f * Time.deltaTime, 1f * Time.deltaTime);
		Quaternion newRotation = Quaternion.Euler(targetAngles); 
		transform.rotation = newRotation;
	}
}
