﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Monk : MonoBehaviour {

    public Transform head;
    public float speed = 5;
    float elapse = 0;
    bool isUp = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        elapse += Time.deltaTime;
        if (elapse >= 5f && !isUp) {
            head.transform.DORotate(new Vector3(0,0,20), 1f).SetEase(Ease.OutQuart);
            elapse = 0;
            isUp = true;
        }
        if (elapse >= 5f && isUp)
        {
            head.transform.DORotate(new Vector3(0, 0, 0), 1f).SetEase(Ease.OutQuart);
            elapse = 0;
            isUp = false;
        }
    }

}
