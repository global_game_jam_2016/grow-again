﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Sun : MonoBehaviour {

    public float loadingTime = 3f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.DORotate(new Vector3(transform.rotation.eulerAngles.x, -270, transform.rotation.eulerAngles.z), loadingTime);  

	}
}
