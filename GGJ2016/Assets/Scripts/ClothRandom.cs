﻿using UnityEngine;
using System.Collections;

public class ClothRandom : MonoBehaviour
{
	public float randomF = 1f;
	public float randomSx = 10f;
	public float randomSy = 10f;

	void Update ()
	{
		float randomX = -100 + randomSx * Mathf.PerlinNoise (Time.time * randomF, 0.0F) * Mathf.Sin (Time.time);
		float randomY = 20 + randomSy * Mathf.PerlinNoise (Time.time * randomF, 0.0F) * Mathf.Sin (Time.time);
		transform.GetComponent<Cloth> ().externalAcceleration = new Vector3 (randomX, randomY, 0);
	}
}