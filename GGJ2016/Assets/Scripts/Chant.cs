﻿using UnityEngine;
using System.Collections;

public class Chant : MonoBehaviour {

	public GameObject chantSymbolPrefab;
	public Sprite[] chantSymbols;
	int orderIndex;
	GameObject currentChantSymbol;
	public static Chant instance;
	
	void Awake () {
		instance = this;
	}
	
	void Update () {
	
	}
	
	public void Sing(Vector3 position, Mover mover, ref int[] chantOrder, ref int currentChantIndex){
		currentChantIndex = chantOrder[orderIndex];
		
		if (currentChantSymbol != null){
			Destroy(currentChantSymbol.gameObject);
		}
		
		currentChantSymbol = Instantiate(chantSymbolPrefab, position, Quaternion.identity) as GameObject;
		currentChantSymbol.GetComponent<ChantSymbol>().mover = mover;
		currentChantSymbol.transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward);
		currentChantSymbol.GetComponent<SpriteRenderer>().sprite = chantSymbols[currentChantIndex];
		
		if (orderIndex < chantOrder.Length-1){
			orderIndex++;
		} else{
			orderIndex = 0;
		}
	}
}
