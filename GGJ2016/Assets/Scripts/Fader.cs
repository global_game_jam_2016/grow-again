﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Fader : MonoBehaviour {

    public static Fader instance;

    public SpriteRenderer black;
    public SpriteRenderer white;

    void Awake()
    {
        instance = this;
        //DontDestroyOnLoad(this);
    }

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.P)) {
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
        }
    }

    public void BlackFadeIn(float time) {
        //if(black.material.color.a>0)
            black.material.DOFade(0, time);
    }

    public void BlackFadeOut(float time)
    {
        //if (black.material.color.a == 0)
            black.material.DOFade(1, time);
    }

    public void WhiteFadeIn(float time)
    {
        //if (white.material.color.a > 0)
            white.material.DOFade(0, time);
    }

    public void WhiteFadeOut(float time)
    {
        //if (white.material.color.a == 0)
            white.material.DOFade(1, time);
    }
}
