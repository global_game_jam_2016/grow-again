﻿using UnityEngine;
using System.Collections;

public class RootTileDirt : Tile {

	public GameObject[] flowerPrefabs;
	GameObject[] myFlowers;
	public ParticleSystem sparkle;
	public bool isTree = true;
	public Tile freeRootTile;
	[HideInInspector]
	public bool hasGrown = false;
	
	void Start(){
	
	}
	
	public override void ConnectRoot(Root root){
		if (freeRootTile != null){
			RootManager.instance.NewRoot(freeRootTile, false);
		}
		
		if (isTree)
			GetComponent<GrowTree>().StartGrowing();
		else
			GrowFlowers();
	}
	
	public void GrowFlowers(){
		
		hasGrown = true;
		RootTileGoal.instance.CheckToOpen();
		
		sparkle.Play();
		
		int flowersToGrow = Random.Range(5, 7);
		myFlowers = new GameObject[flowersToGrow];
		
		for (int i = 0; i < flowersToGrow; ++i){
			GameObject prefab = flowerPrefabs[Random.Range(0, flowerPrefabs.Length)];
			GameObject f = Instantiate(prefab, transform.position, transform.rotation) as GameObject; 
			Flower flower = f.GetComponent<Flower>();
			
			float scale = Random.Range(3f, 5f);
			flower.targetScale = new Vector3(scale, scale, scale);
			//flower.transform.localScale = new Vector3(scale, scale, scale);
			
			float tileSize = 1f;
			float spawnRange = tileSize * .75f;
			
			Vector3 spawnPos = transform.TransformPoint(Random.Range(-spawnRange/2, spawnRange/2), 
			                                            0,
			                                            Random.Range(-spawnRange/2, spawnRange/2));
			flower.transform.position = spawnPos;
			
			myFlowers[i] = flower.gameObject;
		}
	}
}
