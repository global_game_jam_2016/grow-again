using UnityEngine;
using System.Collections;

public class PlayerMover : Mover {
	
	Vector2 input;
	bool hasMoved = false;
	int totalDirt = 5;
	public int dirtGrown = 0;
	public static PlayerMover instance;
	public bool doChant;
	public int[] chantOrder;
	public int currentChantIndex = 0;
	MovingObstacle[] movingObstacles;
	public GameObject halo;
	public bool isPowerOn = false;
	
	void Start () {
		base.Start();
		
		instance = this;
		
		movingObstacles = GameObject.FindObjectsOfType<MovingObstacle>();
		SetPower(isPowerOn);
	}
	
	void Update () {
		base.Update();
		
		if (Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.Space)){
			SetPower(!isPowerOn);
			
			CheckRoot();
		}
		
		input.x = Input.GetAxisRaw ("Horizontal");
		input.y = Input.GetAxisRaw ("Vertical");
		
		if (input.x != 0 && !hasMoved) {
			Move (new Vector2 (input.x, 0));
			hasMoved = true;
		} else if (input.y != 0 && !hasMoved) {
			Move (new Vector2 (0, input.y));
			hasMoved = true;
		} else if (input.x == 0 && input.y == 0) {
			hasMoved = false;
		}
	}
	
	public void CheckRoot(){
		if (targetTile){
			if (targetTile.isRooted){
				RootManager.instance.SelectRoot(targetTile);
			}
		}
	}
	
	public void SetPower(bool set){
		isPowerOn = set;
		halo.SetActive(isPowerOn);
		CheckRoot();
		
		if (!set){
			RootManager.instance.DeselectRoot();
		}
	}
	
	public override void OnRotate ()
	{
		base.OnRotate ();
		CameraControl.instance.Rotate (moveInput);
	}
	
	public override void OnNewTile ()
	{
		base.OnNewTile ();
		
		LevelManager.instance.Step();
		
		//currentTile.canMoveOnto = false;
		//lastTile.canMoveOnto = true;
		
		MoveObstacles();
		
		//if (lastTile.isRooted && isPowerOn){
			currentTile.AttemptRootGrow();
		//}
	}
	
	void MoveObstacles(){
		
		for (int i = 0; i < movingObstacles.Length; ++i) {
			
			switch (movingObstacles[i].moveType){
			case MovingObstacle.MoveType.onPlayerMove: movingObstacles[i].Go(); break;
			case MovingObstacle.MoveType.mirrorPlayer: movingObstacles[i].Move(new Vector2(-moveInput.x, -moveInput.y)); break;
			}

		}
	}
	
	public override void OnLeaveTile ()
	{	
		if (doChant){
			Chant.instance.Sing (visualTargetPos + transform.up * gridSize, this, ref chantOrder, ref currentChantIndex);
		}
		
	}
}
