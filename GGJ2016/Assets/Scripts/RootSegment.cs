﻿using UnityEngine;
using System.Collections;

public class RootSegment : MonoBehaviour {
	public GameObject cube;
	public float GrowTime;
	public Vector3 TipLocalOffset;
	float curTime = 0;
	public void Grow(Transform start, Transform end)
	{
		StartCoroutine(GrowE(start,end));
	}
	IEnumerator GrowE(Transform start,Transform end)
	{
		Vector3 offset = start.InverseTransformPoint(end.position);
		float yOffset = offset.y;
		offset.y=0;

		GameObject obj = Instantiate(cube,start.position,Quaternion.LookRotation(start.TransformDirection(offset),start.up)) as GameObject;
		obj.transform.SetParent(transform);
		yield return StartCoroutine(GrowE(obj.transform,GrowTime));
		if(yOffset != 0)
		{
			offset = start.TransformPoint(offset);
			obj = Instantiate(cube,offset,Quaternion.LookRotation(end.position-offset,end.up)) as GameObject;
			obj.transform.SetParent(transform);
			StartCoroutine(GrowE(obj.transform,GrowTime));
		}
	}
	IEnumerator GrowE(Transform trans, float time)
	{
		trans.localScale = new Vector3 (1, 1, 0.1f);
		while (trans.localScale != Vector3.one) {
			curTime += Time.deltaTime;
			trans.localScale = Vector3.Lerp(trans.localScale,Vector3.one,curTime/GrowTime);
			yield return null;
		}
		trans.localScale = Vector3.one;
	}
}
