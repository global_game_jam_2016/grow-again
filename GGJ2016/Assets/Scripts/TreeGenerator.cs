﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TreeGenerator : MonoBehaviour
{

	public GameObject branch;
	public GameObject leaf;

	public int treeH = 10;
	public int trunkH = 4;
	private int branchA = 4;
	public int branchL = 6;

	public float topScale = 3.5f;

	public float growSpeed = 0.05f;

	int vibrate = 7;

	public float treeScale = 0.1f;
	public float leafScale = 0.3f;

	public Vector3 dir;// = Vector3.left;
	public Transform myTileTransform;
	public float addRotation = 0;
	
	// Use this for initialization
	void Start ()
	{
	}


	public IEnumerator Generator ()
	{
		branch.transform.localScale = new Vector3 (treeScale, treeScale, treeScale);
		leaf.transform.localScale = new Vector3 (leafScale, leafScale, leafScale);
		topScale = topScale * treeScale;
//		branch.transform.localScale = branch.transform.localScale * leafScale;
//		leaf.transform.localScale = leaf.transform.localScale * treeScale;
		Quaternion tileRotation = myTileTransform.transform.rotation;
		for (int i = 0; i< treeH; i++) {
			
			GameObject go = Instantiate (branch, new Vector3 (transform.position.x, 
			                                                  transform.position.y + i * branch.transform.localScale.y, 
			                                                  transform.position.z), Quaternion.identity) as GameObject;
			go.GetComponent<Renderer> ().enabled = false;
			yield return new WaitForSeconds (growSpeed);

			go.transform.SetParent (transform);
			if (i == treeH - 1) {
				GameObject top = Instantiate (leaf, new Vector3 (go.transform.position.x, 
				                                                 go.transform.position.y + 0.5f * branch.transform.localScale.y + 0.5f * leaf.transform.localScale.y, 
				                                                 go.transform.position.z), Quaternion.identity) as GameObject;
				top.transform.localScale += new Vector3 (topScale, topScale, topScale);
				top.transform.localPosition += new Vector3 (0, topScale / 4, 0);
				top.transform.DOPunchScale (new Vector3 (0.5f, 0.5f, 0.5f) * treeScale, 1f, vibrate - 1, 0.5f);
				top.transform.SetParent (transform);
				top.transform.RotateAround (transform.position, dir, 90 + addRotation);
			}
			go.GetComponent<Renderer> ().enabled = true;
			go.transform.RotateAround (transform.position, dir, 90 + addRotation);
		}
			
		for (int k = 0; k < branchA; k++) {

			int hight = Random.Range (1, treeH - trunkH); // 1,2,3,4,5
//			int dir = Random.Range (1, 5); // 1,2,3,4


			switch (k % branchA + 1) {
			case 1:
				for (int j = 1; j <= branchL-hight/2; j++) {
					GameObject go = Instantiate (branch, new Vector3 (transform.position.x + j * branch.transform.localScale.x, 
					                                  transform.position.y + (hight + trunkH) * branch.transform.localScale.y,
							                          transform.position.z), Quaternion.identity) as GameObject;
					go.GetComponent<Renderer> ().enabled = false;
					yield return new WaitForSeconds (growSpeed);
					go.transform.SetParent (transform);
					go.transform.RotateAround (transform.position, dir, 90 + addRotation);
					go.GetComponent<Renderer> ().enabled = true;
				}
				for (int j = 1; j <= 2; j++) {
					GameObject go = Instantiate (branch, new Vector3 (transform.position.x + (branchL - hight / 2) * branch.transform.localScale.x,
					                                  transform.position.y + (hight + trunkH) * branch.transform.localScale.y + j * branch.transform.localScale.y,
					                                  transform.position.z), Quaternion.identity) as GameObject;
					go.GetComponent<Renderer> ().enabled = false;
					yield return new WaitForSeconds (growSpeed);
					go.transform.SetParent (transform);
					if (j == 2) {
						GameObject top = Instantiate (leaf, new Vector3 (go.transform.position.x, 
						                                                 go.transform.position.y + 0.5f * branch.transform.localScale.y + 0.5f * leaf.transform.localScale.y, 
						                                                 go.transform.position.z), Quaternion.identity) as GameObject;
						top.transform.DOPunchScale (new Vector3 (0.5f, 0.5f, 0.5f) * treeScale, 1f, vibrate, 0.5f);
						top.transform.SetParent (transform);
						top.transform.RotateAround (transform.position, dir, 90 + addRotation);
					}
					go.GetComponent<Renderer> ().enabled = true;
					go.transform.RotateAround (transform.position, dir, 90 + addRotation);

				}
				break;
			case 2:
				for (int j = 1; j <= branchL-hight/2; j++) {
					GameObject go = Instantiate (branch, new Vector3 (transform.position.x - j * branch.transform.localScale.x, 
					                                  transform.position.y + (hight + trunkH) * branch.transform.localScale.y,
							                          transform.position.z), Quaternion.identity) as GameObject;
					go.GetComponent<Renderer> ().enabled = false;
					yield return new WaitForSeconds (growSpeed);
					go.transform.SetParent (transform);
					go.transform.RotateAround (transform.position, dir, 90 + addRotation);
					go.GetComponent<Renderer> ().enabled = true;

				}
				for (int j = 1; j <= 2; j++) {
					GameObject go = Instantiate (branch, new Vector3 (transform.position.x - (branchL - hight / 2) * branch.transform.localScale.x,
					                                  transform.position.y + (hight + trunkH) * branch.transform.localScale.y + j * branch.transform.localScale.y,
					                                  transform.position.z), Quaternion.identity) as GameObject;
					go.GetComponent<Renderer> ().enabled = false;
					yield return new WaitForSeconds (growSpeed);
					go.transform.SetParent (transform);

					if (j == 2) {
						GameObject top = Instantiate (leaf, new Vector3 (go.transform.position.x, 
						                                                 go.transform.position.y + 0.5f * branch.transform.localScale.y + 0.5f * leaf.transform.localScale.y, 
						                                                 go.transform.position.z), Quaternion.identity) as GameObject;
						top.transform.DOPunchScale (new Vector3 (0.5f, 0.5f, 0.5f) * treeScale, 1f, vibrate, 0.5f);
						top.transform.SetParent (transform);
						top.transform.RotateAround (transform.position, dir, 90 + addRotation);
					}
					go.GetComponent<Renderer> ().enabled = true;
					go.transform.RotateAround (transform.position, dir, 90 + addRotation);
				}
				break;
			case 3:
				for (int j = 1; j <= branchL-hight/2; j++) {
					GameObject go = Instantiate (branch, new Vector3 (transform.position.x, 
					                                  transform.position.y + (hight + trunkH) * branch.transform.localScale.y,
					                                  transform.position.z + j * branch.transform.localScale.z), Quaternion.identity) as GameObject;
					go.GetComponent<Renderer> ().enabled = false;
					yield return new WaitForSeconds (growSpeed);
					go.transform.SetParent (transform);
					go.transform.RotateAround (transform.position, dir, 90 + addRotation);
					go.GetComponent<Renderer> ().enabled = true;

				}
				for (int j = 1; j <= 2; j++) {
					GameObject go = Instantiate (branch, new Vector3 (transform.position.x,
					                                  transform.position.y + (hight + trunkH) * branch.transform.localScale.y + j * branch.transform.localScale.y,
					                                  transform.position.z + (branchL - hight / 2) * branch.transform.localScale.z), Quaternion.identity) as GameObject;
					go.GetComponent<Renderer> ().enabled = false;
					yield return new WaitForSeconds (growSpeed);
					go.transform.SetParent (transform);
					if (j == 2) {
						GameObject top = Instantiate (leaf, new Vector3 (go.transform.position.x, 
						                                                 go.transform.position.y + 0.5f * branch.transform.localScale.y + 0.5f * leaf.transform.localScale.y, 
						                                                 go.transform.position.z), Quaternion.identity) as GameObject;
						top.transform.DOPunchScale (new Vector3 (0.5f, 0.5f, 0.5f) * treeScale, 1f, vibrate, 0.5f);
						top.transform.SetParent (transform);
						go.GetComponent<Renderer> ().enabled = false;
						top.transform.RotateAround (transform.position, dir, 90 + addRotation);
					}
					go.GetComponent<Renderer> ().enabled = true;
					go.transform.RotateAround (transform.position, dir, 90 + addRotation);
				}
				break;
			case 4:
				for (int j = 1; j <= branchL - hight/2; j++) {
					GameObject go = Instantiate (branch, new Vector3 (transform.position.x, 
					                                  transform.position.y + (hight + trunkH) * branch.transform.localScale.y,
					                                  transform.position.z - j * branch.transform.localScale.z), Quaternion.identity) as GameObject;
					go.GetComponent<Renderer> ().enabled = false;
					yield return new WaitForSeconds (growSpeed);
					go.transform.SetParent (transform);
					go.transform.RotateAround (transform.position, dir, 90 + addRotation);
					go.GetComponent<Renderer> ().enabled = true;

				}
				for (int j = 1; j <= 2; j++) {
					GameObject go = Instantiate (branch, new Vector3 (transform.position.x,
					                                  transform.position.y + (hight + trunkH) * branch.transform.localScale.y + j * branch.transform.localScale.y,
					                                  transform.position.z - (branchL - hight / 2) * branch.transform.localScale.z), Quaternion.identity) as GameObject;
					go.GetComponent<Renderer> ().enabled = false;
					yield return new WaitForSeconds (growSpeed);
					go.transform.SetParent (transform);
					if (j == 2) {
						GameObject top = Instantiate (leaf, new Vector3 (go.transform.position.x, 
						                                                 go.transform.position.y + 0.5f * branch.transform.localScale.y + 0.5f * leaf.transform.localScale.y, 
						                                                 go.transform.position.z), Quaternion.identity) as GameObject;
						top.transform.DOPunchScale (new Vector3 (0.5f, 0.5f, 0.5f) * treeScale, 1f, vibrate, 0.5f);
						top.transform.SetParent (transform);
						top.transform.RotateAround (transform.position, dir, 90 + addRotation);
					}
					go.GetComponent<Renderer> ().enabled = true;
					go.transform.RotateAround (transform.position, dir, 90 + addRotation);
				}
				break;
			}
		}
	}

	public void Grow ()
	{
		StartCoroutine ("Generator");
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Space)) {	
			//Generator ();
			StartCoroutine ("Generator");
		}

//		if (Input.GetKeyDown (KeyCode.Q)) {	
//			Application.LoadLevel (Application.loadedLevel);
//		}
	}
}
