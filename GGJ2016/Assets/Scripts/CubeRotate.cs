﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CubeRotate : MonoBehaviour
{
	public float duration = 1f;
	public Transform parentTransform;

	bool isRotating = false;
	bool isSpinning = false;

	Quaternion origin;


	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		RotateCube ();
		FreeSpinning ();
	}

	void RotateCube ()
	{
		if (isRotating == false) {
			if (Input.GetKeyDown (KeyCode.LeftArrow)) {
				isRotating = true;
				transform.DORotate (new Vector3 (0, 90, 0), duration, RotateMode.WorldAxisAdd).SetEase (Ease.InOutExpo).OnComplete (FinishRotate);
			}
			if (Input.GetKeyDown (KeyCode.RightArrow)) {
				isRotating = true;
				transform.DORotate (new Vector3 (0, -90, 0), duration, RotateMode.WorldAxisAdd).SetEase (Ease.InOutExpo).OnComplete (FinishRotate);
			}
			if (Input.GetKeyDown (KeyCode.UpArrow)) {
				isRotating = true;
				transform.DORotate (new Vector3 (90, 0, 0), duration, RotateMode.WorldAxisAdd).SetEase (Ease.InOutExpo).OnComplete (FinishRotate);
			}
			if (Input.GetKeyDown (KeyCode.DownArrow)) {
				isRotating = true;
				transform.DORotate (new Vector3 (-90, 0, 0), duration, RotateMode.WorldAxisAdd).SetEase (Ease.InOutExpo).OnComplete (FinishRotate);
			}
		}
	}

	void FreeSpinning ()
	{
		if (Input.GetKeyDown (KeyCode.Z) && isSpinning == false) {
			origin = transform.rotation;
			isSpinning = true;
			transform.DORotate (new Vector3 (transform.rotation.eulerAngles.x + (Mathf.Rad2Deg * (Mathf.Atan (1 / Mathf.Sqrt (2)))),
			                                 transform.rotation.eulerAngles.y, 
			                                 transform.rotation.eulerAngles.z + 45), duration, RotateMode.WorldAxisAdd);//.SetEase (Ease.InOutExpo);//.OnComplete (FinishRotate);
			transform.SetParent (parentTransform);
		}
		if (isSpinning == true) {
			parentTransform.Rotate (0, 20 * Time.deltaTime, 0);
		}
		if (Input.GetKeyDown (KeyCode.X) && isSpinning == true) {
			isSpinning = false;
			transform.DORotate (origin.eulerAngles, duration, RotateMode.Fast);

		}
	}

	void FinishRotate ()
	{
		isRotating = !isRotating;
	}


}
