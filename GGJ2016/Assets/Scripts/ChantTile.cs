﻿using UnityEngine;
using System.Collections;

public class ChantTile : Tile {

	public int chantIndex;
	public Color activeCol;
	public SpriteRenderer sprite;
	public bool isActive = false;
	void Start () {
		sprite = GetComponentInChildren<SpriteRenderer>();
		SetChantSymbol();
	}
	
	public void SetChantSymbol(){
		sprite.sprite = Chant.instance.chantSymbols[chantIndex];
		sprite.color = Color.grey;
	}
	
	public override void MoveOn(Mover.MoverType moverType){
		if (!isActive && chantIndex == PlayerMover.instance.currentChantIndex){
			Activate();
		}
	}
	
	void Activate(){
		LevelManager.instance.ChantCount(1);
		print ("activate tile");
		sprite.color = activeCol;
		isActive = true;
	}
	
	void Update () {
	
	}
}
