﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Win : MonoBehaviour {

	public float duration = 1f;
	public Transform parentTransform;
	public GameObject player;
	public GameObject playerV;
	
	bool isSpinning = false;
	
	Quaternion origin;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (isSpinning){
			Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(0, 0, -10), 20*Time.deltaTime);
			Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, Quaternion.Euler(new Vector3(0, 0, 0)), 20*Time.deltaTime);
			parentTransform.Rotate (0, 20 * Time.deltaTime, 0);
		}
	}
	
	public void StartSpinning(){
		isSpinning = true;
		
		if(player == null){
			player = GameObject.Find("Player(Clone)") as GameObject;
			playerV = GameObject.Find("PlayerVis(Clone)") as GameObject;
			
		}
		player.GetComponent<PlayerMover>().enabled = false;
		origin = transform.rotation;
		player.transform.SetParent(transform);
		playerV.transform.SetParent(transform);
		isSpinning = true;
		transform.DORotate (new Vector3 (transform.rotation.eulerAngles.x + (Mathf.Rad2Deg * (Mathf.Atan (1 / Mathf.Sqrt (2)))),
		                                 transform.rotation.eulerAngles.y, 
		                                 transform.rotation.eulerAngles.z + 45), duration, RotateMode.WorldAxisAdd);//.SetEase (Ease.InOutExpo);//.OnComplete (FinishRotate);
		transform.SetParent (parentTransform);
	}
}
