﻿using UnityEngine;
using System.Collections;

public class SelfSpinning : MonoBehaviour
{

	public float daySpeed = 5f;
	public float yearSpeed = 1f;

	void Start ()
	{
	
	}
	
	void Update ()
	{
		transform.Rotate (daySpeed * Time.deltaTime, daySpeed * Time.deltaTime, daySpeed * Time.deltaTime);
		transform.RotateAround (Vector3.zero, Vector3.up, yearSpeed * Time.deltaTime);
	}
}
