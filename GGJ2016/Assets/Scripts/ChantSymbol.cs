﻿using UnityEngine;
using System.Collections;

public class ChantSymbol : MonoBehaviour {

	public float fadeSpeed = 7.0f;
	Renderer r;
	[HideInInspector]
	public Mover mover;
	
	void Start () {
		r = GetComponent<Renderer>();
	}
	
	void Update () {
		if (mover.visual)
			transform.position = mover.visual.transform.position + mover.transform.up*(mover.gridSize*1.5f);
		transform.forward = mover.transform.up;
		//transform.right = Camera.main.transform.right;
		//transform.rotation = Quaternion.FromToRotation(Vector3.up, mover.currentTile.transform.forward);
		
		Color col = r.material.color;
		col.a -= Time.deltaTime * fadeSpeed;
		if (col.a <= 0){
			Destroy(this.gameObject);
		}
		r.material.color = col;
	}
}
