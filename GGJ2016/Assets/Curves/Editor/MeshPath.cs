﻿using UnityEngine;
using System.Collections;

public class MeshPath {

	//Possibly add functionality later
	public Vector3[] Shape;
	public bool Loop;

	public Mesh ConstructLinearPath(Vector3[] Points)
	{

		Quaternion[] quats = GenerateDirections (Points);
		
		Mesh mesh = new Mesh ();
		mesh.name = "MeshPath";

		mesh.vertices = GenerateVertices(Points,quats,1,0.2f);
		mesh.normals = GenerateNormals (mesh.vertices.Length);
		mesh.triangles = GenerateTris(mesh.vertices.Length/Shape.Length,Loop);
		return mesh;
	}
	public Quaternion[] GenerateDirections (Vector3[] Points)
	{
		
		int segments = Points.Length;
		Quaternion[] quats = new Quaternion[segments];

		//First point
		if (Loop) {
			Vector3 v1 = Points[segments-1]-Points[0];
			Vector3 v2 = Points[0]-Points[1];
			quats[0] =Quaternion.LookRotation(Vector3.Lerp(v1,v2,0.5f));
		} else {
			Vector3 v1 = Points[0]-Points[1];
			quats [0] = Quaternion.LookRotation(v1);
		}
		
		for (int i = 1; i < segments - 1; ++i) {
			Vector3 v1 = Points[i-1]-Points[i];
			Vector3 v2 = Points[i]-Points[i+1];
			quats[i] =Quaternion.LookRotation(Vector3.Lerp(v1,v2,0.5f));
		}
		//Last Point
		if (Loop) {
			Vector3 v1 = Points[segments-2]-Points[segments-1];
			Vector3 v2 = Points[segments-1]-Points[0];
			quats[segments - 1] =Quaternion.LookRotation(Vector3.Lerp(v1,v2,0.5f));
		} else {
			Vector3 v1 = Points[Points.Length-2]-Points[Points.Length-1];
			quats [segments - 1] = Quaternion.LookRotation(v1);
		}
		return quats;
	}
	public Vector3[] GenerateVertices(Vector3[] Points,Quaternion[] Directions)
	{
		return GenerateVertices (Points, Directions, 1, 1);
	}
	public Vector3[] GenerateVertices(Vector3[] Points,Quaternion[] Directions, float startSize, float EndSize)
	{
		Vector3[] verts;
		int shapeCount = Shape.Length;
		verts = new Vector3[Points.Length * shapeCount];
		
		
		for (int i = 0; i < Points.Length; ++i) {
			float scale = Mathf.Lerp(startSize,EndSize,i/((float)Points.Length));
			int v = i * shapeCount;
			for (int s = 0; s < shapeCount; ++s) {
				verts [v + s] = Points [i] + Directions [i] * Shape[s] * scale;
			}
		}
		return verts;
	}
	public int[] GenerateTris(int segments, bool loop)
	{
		int shapeCount = Shape.Length;
		int triCount = shapeCount * 2;

		int[] tris;
		if(Loop)
		{
			tris = new int[(segments) * triCount * 3];
			++segments;
		}
		else
		{
			tris = new int[(segments-1) * triCount * 3];
		}
		//For each segment, calculate the tris
		for (int iSeg = 0; iSeg < segments - 1; ++iSeg) 
		{
			int lOffset = iSeg * shapeCount * 6;
			int rOffset = iSeg * shapeCount;

			for (int a = 0; a < shapeCount; ++a) 
			{
				if(Loop)
				{
					int mod = (segments-1)*shapeCount;
					tris[lOffset + 6 * a] = (rOffset + a)% mod;
					tris[lOffset + 6 * a + 1] = (rOffset + (a + 1)%shapeCount)%mod;
					tris[lOffset + 6 * a + 2] = (rOffset + a + shapeCount)%mod;

					tris[lOffset + 6 * a + 3] = (rOffset + a + shapeCount)%mod;
					tris[lOffset + 6 * a + 4] = (rOffset + (a + 1)%shapeCount)%mod;
					tris[lOffset + 6 * a + 5] = (rOffset + (a + 1)%shapeCount + shapeCount)%mod;
				}
				else
				{
					tris[lOffset + 6 * a] = (rOffset + a);
					tris[lOffset + 6 * a + 1] = (rOffset + (a + 1)%shapeCount);
					tris[lOffset + 6 * a + 2] = (rOffset + a + shapeCount);
					
					tris[lOffset + 6 * a + 3] = (rOffset + a + shapeCount);
					tris[lOffset + 6 * a + 4] = (rOffset + (a + 1)%shapeCount);
					tris[lOffset + 6 * a + 5] = (rOffset + (a + 1)%shapeCount + shapeCount);
				}
			}
		}
		return tris;
	}
	public Vector3[] GenerateNormals2(int segments)
	{
		Vector3[] normals = new Vector3[segments*2];
		for(int i = 0; i < normals.Length; ++i)
		{
			normals[i] = Vector3.up;
		}
		return normals;
	}
	public Vector2[] GenerateUV2(int segments)
	{
		Vector2[] uv = new Vector2[segments * 2];
		for (int i = 0; i < segments; ++i) {
			
		}
		return uv;
	}

	public Vector3[] GenerateNormals(int count)
	{
		Vector3[] normals = new Vector3[count];
		for(int i = 0; i < normals.Length; ++i)
		{
			normals[i] = Vector3.up;
		}
		return normals;
	}



	/// <summary>
	/// /////OLD IMPLEMENTATION
	/// </summary>
	/// <returns>The linear box path.</returns>
	/// <param name="Points">Points.</param>
	/// <param name="halfWidth">Half width.</param>
	/// <param name="halfHeight">Half height.</param>





	public Mesh ContructLinearBoxPath(Vector3[] Points, float halfWidth = 3, float halfHeight = 1)
	{
		int segments = Points.Length;


		Quaternion[] quats = new Quaternion[segments];

		//First point
		if (Loop) {
			Vector3 v1 = Points[segments-1]-Points[0];
			Vector3 v2 = Points[0]-Points[1];
			quats[0] =Quaternion.LookRotation(Vector3.Lerp(v1,v2,0.5f));
		} else {
			quats [segments - 1] = quats [segments - 2];
		}

		for (int i = 1; i < segments - 1; ++i) {
			Vector3 v1 = Points[i-1]-Points[i];
			Vector3 v2 = Points[i]-Points[i+1];
			quats[i] =Quaternion.LookRotation(Vector3.Lerp(v1,v2,0.5f));
		}
		//Last Point
		if (Loop) {
			Vector3 v1 = Points[segments-2]-Points[segments-1];
			Vector3 v2 = Points[segments-1]-Points[0];
			quats[segments - 1] =Quaternion.LookRotation(Vector3.Lerp(v1,v2,0.5f));
		} else {
			quats [segments - 1] = quats [segments - 2];
		}


		Mesh mesh = new Mesh ();
		mesh.name = "MeshPath";
		
		mesh.vertices = GenerateBoxVertices(Points,quats,halfWidth,halfHeight);
		mesh.normals = GenerateBoxNormals (mesh.vertices.Length/2);
		mesh.triangles = GenerateBoxTris(mesh.vertices.Length/2);
		//mesh.uv = GenerateUV();
		return mesh;
	}
	public Vector3[] GenerateBoxVertices(Vector3[] Points,Quaternion[] Directions , float halfWidth, float halfHeight)
	{
		Vector3[] verts;
		if (Loop) {
			verts = new Vector3[(Points.Length + 1) * 2];
		} else {
			verts = new Vector3[Points.Length * 2];
		}
		for(int i = 0; i < Points.Length; ++i)
		{
			int v = i * 2;
			verts[v] = Points[i] + Directions[i] * Vector3.left * halfWidth;
			verts[v+1] = Points[i] + Directions[i] * Vector3.right * halfWidth;
		}
		if (Loop) {
			verts[verts.Length-2] = Points[0] + Directions[0] * Vector3.left * halfWidth;
			verts[verts.Length-1] = Points[0] + Directions[0] * Vector3.right * halfWidth;
		}
		return verts;
	}
	public Vector3[] GenerateBoxVertices(Vector3[] Points, float halfWidth, float halfHeight)
	{
		Vector3[] verts = new Vector3[Points.Length * 2];
		for(int i = 0; i < Points.Length; ++i)
		{
			int v = i * 2;
			verts[v] = Points[i] + Vector3.left * halfWidth;
			verts[v+1] = Points[i] + Vector3.right * halfWidth;
		}
		return verts;
	}
	public int[] GenerateBoxTris(int segments)
	{
		int[] tris = new int[(segments-1) * 6];
		for (int i = 0; i < segments-1; ++i) {

			int t = i * 6;
			int v = i * 2;

			tris[t] = v+0;
			tris[t+1] = v+1;
			tris[t+2] = v+2;
			tris[t+3] = v+2;
			tris[t+4] = v+1;
			tris[t+5] = v+3;
		}
		return tris;
	}
	public Vector3[] GenerateBoxNormals(int segments)
	{
		Vector3[] normals = new Vector3[segments*2];
		for(int i = 0; i < normals.Length; ++i)
		{
			normals[i] = Vector3.up;
		}
		return normals;
	}
	public Vector2[] GenerateUV(int segments)
	{
		Vector2[] uv = new Vector2[segments * 2];
		for (int i = 0; i < segments; ++i) {

		}
		return uv;
	}
}
