﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(Curve),true)]
[CanEditMultipleObjects]
public class CurveInspector : Editor {

	enum SelectMode
	{
		Invalid,
		ControlPoint,
		EditPoint
	}
	SelectMode sMode = SelectMode.Invalid;

	public override void OnInspectorGUI ()
	{
		SerializedProperty points = serializedObject.FindProperty ("points");
		SerializedProperty basePoints = serializedObject.FindProperty ("basePoints");
		SerializedProperty controlPoints = serializedObject.FindProperty ("controlPoints");
		SerializedProperty editPoints = serializedObject.FindProperty ("editPoints");
		SerializedProperty degree = serializedObject.FindProperty ("degree");
		SerializedProperty loop = serializedObject.FindProperty ("loop");
		SerializedProperty resolution = serializedObject.FindProperty ("resolution");
		SerializedProperty meshResolution = serializedObject.FindProperty ("meshResolution");

		//Path
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("ConstructPath")) {
			ConstructPathFromCurve((Curve)target);
		}
		resolution.intValue = EditorGUILayout.IntField ("Resolution", resolution.intValue);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("GenerateMesh")) {
			ConstructMeshFromCurve((Curve)target);
		}
		meshResolution.intValue = EditorGUILayout.IntField ("Resolution", meshResolution.intValue);
		EditorGUILayout.EndHorizontal ();

		sMode = (SelectMode)EditorGUILayout.EnumPopup ("Selection Mode",sMode);
		
		degree.intValue = Mathf.Max (1, EditorGUILayout.IntField ("Degree",degree.intValue));
		loop.boolValue = EditorGUILayout.Toggle ("Loop",loop.boolValue);

		if (GUILayout.Button ("Clear All Points")) {
			controlPoints.arraySize = 0;
			basePoints.arraySize = 0;
			editPoints.arraySize = 0;
			SceneView.RepaintAll();
			
		}

		if (sMode == SelectMode.ControlPoint) {
			if(GUILayout.Button("Recalculate"))
			{
				serializedObject.ApplyModifiedProperties ();
				Curve curve = ((Curve)target);
				curve.RecalculateEditPoints();
				EditorUtility.SetDirty(curve);
			}
			EditorGUILayout.PropertyField (controlPoints, new GUIContent ("Points"), true);
		}
		if (sMode == SelectMode.EditPoint) {
			if(GUILayout.Button("Recalculate"))
			{
				serializedObject.ApplyModifiedProperties ();
				Curve curve = ((Curve)target);
				curve.RecalculateControlPoints();
				EditorUtility.SetDirty(curve);
			}
			EditorGUILayout.PropertyField (editPoints, new GUIContent ("Points"), true);
		}
		serializedObject.ApplyModifiedProperties ();
		serializedObject.UpdateIfDirtyOrScript ();

	}

	void OnSceneGUI() 
	{

		if (sMode != SelectMode.Invalid) {
			if (Event.current.type == EventType.MouseDown && Event.current.button == 0 && !(Event.current.alt || Event.current.command || Event.current.control)) {
				PlacePoint (Event.current);
			}
			if (Event.current.type == EventType.Layout) {
				HandleUtility.AddDefaultControl (0);
			}
		}
	}


	void PlacePoint(Event e)
	{
		Curve curve = (Curve)target;
		Camera cam = Camera.current;
		if (cam == null) {
			return;
		}
		Vector3 initialPoint = cam.transform.InverseTransformPoint (curve.transform.position);
		if (cam.orthographic) {
			Vector3 NextPoint = cam.ScreenToWorldPoint (new Vector3 (e.mousePosition.x, cam.pixelHeight - e.mousePosition.y, initialPoint.z));
			NextPoint = Vector3.ProjectOnPlane (NextPoint, cam.transform.forward);
			if (sMode == SelectMode.ControlPoint) {
				curve.AddControlPoint(NextPoint);
			} else if (sMode == SelectMode.EditPoint) {
				curve.AddEditPoint(NextPoint);
			}
		} else {
			Vector3 NextPoint = cam.ScreenToWorldPoint (new Vector3 (e.mousePosition.x, cam.pixelHeight - e.mousePosition.y, initialPoint.z));
			
			if (sMode == SelectMode.ControlPoint) {
				curve.AddControlPoint(NextPoint);
			} else if (sMode == SelectMode.EditPoint) {
				curve.AddEditPoint(NextPoint);
			}
		}
		EditorUtility.SetDirty (curve);
		Repaint ();
		SceneView.RepaintAll ();
	}
	void ConstructPathFromCurve(Curve curve)
	{
		Path path = curve.GetComponent<Path> ();
		if (!path) {
			path = curve.gameObject.AddComponent<Path> ();
		}
		if (curve.degree > 1) {
			Vector3[] points = new Vector3[curve.resolution + 1];
			float delta = Mathf.Max (1.0f / (curve.resolution + 1), 0.00001f);
			Handles.color = Color.cyan;
			float t = 0;
			for (int i = 0; i < curve.resolution+1; ++i) {
				points [i] = curve.GetPointAt (t);
				t += delta;
			}
			points [curve.resolution] = curve.GetPointAt (1);
			path.ConstructPath (points);
		} else {
			path.ConstructPath (curve.editPoints.ToArray ());
		}
	}
	void ConstructMeshFromCurve(Curve curve)
	{
		MeshPath mp = new MeshPath();
		mp.Loop = curve.loop;
		mp.Shape = new Vector3[]{
			new Vector2(-.1f,-.1f),
			new Vector2(-.15f,0f),
			new Vector2(-.1f,.1f),
			new Vector2(.1f,.1f),
			new Vector2(.15f,.0f),
			new Vector2(.1f,-.1f),
		};
		
		
		MeshFilter filter = curve.GetComponent<MeshFilter> ();
		if (!filter) {
			filter = curve.gameObject.AddComponent<MeshFilter>();
		}
		MeshCollider collider = curve.GetComponent<MeshCollider>();
		Vector3[] points;
		if (curve.degree > 1) {
			points = new Vector3[curve.meshResolution + 1];
			if (curve.meshResolution >= 1) {
				float delta = Mathf.Max (1.0f / (curve.meshResolution + 1), 0.00001f);
				Handles.color = Color.cyan;
				float t = 0;
				for (int i = 0; i < curve.meshResolution+1; ++i) {
					points [i] = curve.GetPointAt (t);
					t += delta;
				}
				points [curve.meshResolution] = curve.GetPointAt (1);
				mp.ConstructLinearPath (points);
			}
		} else {
			points = curve.editPoints.ToArray();
		}
		
		
		
		
		if(filter.sharedMesh != null)
		{
			DestroyImmediate(filter.sharedMesh);
		}
		filter.mesh = mp.ConstructLinearPath (points);
		if(collider)
		{
			collider.sharedMesh = filter.sharedMesh;
		}
	}

	[DrawGizmo(GizmoType.Selected,typeof(Curve))]
	static void OnDrawGizmos(Curve curve,GizmoType type)
	{
		Handles.color = Color.red;
		DrawCaps (curve.basePoints.ToArray(),0.025f);
		Handles.color = Color.yellow;
		DrawCaps (curve.controlPoints.ToArray (),0.025f);
		DrawLines (curve.controlPoints.ToArray ());

		if (curve.controlPoints.Count > curve.degree) {
			Handles.color = Color.cyan;
			DrawCaps (curve.editPoints.ToArray (),0.025f);
	
			Vector3[] curvePoints = new Vector3[curve.resolution + 1];
			float delta = Mathf.Max (1.0f / curve.resolution, 0.00001f);
			float t = 0;
			for (int i = 0; i < curvePoints.Length - 1; ++i) {
				curvePoints [i] = curve.GetPointAt (t);
				t += delta;
			}
			curvePoints [curvePoints.Length - 1] = curve.GetPointAt (t);
			DrawLines (curvePoints);
		}
	}
	static void DrawLines(Vector3[] points)
	{
		for(int i = 0; i < points.Length-1; ++i)
		{
			Handles.DrawLine(points[i],points[i+1]);
		}
	}
	static void DrawCaps(Vector3[] points, float size = 1)
	{
		for(int i = 0; i < points.Length; ++i)
		{
			Handles.DotCap(0,points[i],Quaternion.identity,size);
		}
	}

}
