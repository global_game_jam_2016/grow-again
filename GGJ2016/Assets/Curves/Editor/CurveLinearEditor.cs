﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
/*
[CustomEditor(typeof(CurveLinear))]
public class CurveLinearEditor : Editor {
	
	Vector3 Normal;
	enum PlaceMode
	{
		Vec,
		Cam
	}
	enum SelectMode
	{
		Invalid,
		ControlPoint,
		EditPoint
	}
	PlaceMode pMode = PlaceMode.Cam;
	SelectMode sMode = SelectMode.Invalid;
	public override void OnInspectorGUI()
	{
		serializedObject.UpdateIfDirtyOrScript ();
		Curve curve = (Curve)target;
		
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("ConstructPath")) {
			ConstructPathFromCurve(curve);
		}
		curve.resolution = EditorGUILayout.IntField ("Resolution Multiplier", curve.resolution);
		EditorGUILayout.EndHorizontal ();
		
		EditorGUILayout.BeginHorizontal ();
		
		
		curve.meshResolution = EditorGUILayout.IntField ("Resolution Multiplier", curve.meshResolution);
		if (GUILayout.Button ("GenerateMesh")) {
			ConstructMeshFromCurve(curve);
		}
		EditorGUILayout.EndHorizontal ();
		
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Off")) {
			sMode = SelectMode.Invalid;
		}
		if (GUILayout.Button ("Curve Point")) {
			sMode = SelectMode.ControlPoint;
		}
		if (GUILayout.Button ("Edit Point")) {
			sMode = SelectMode.EditPoint;
		}
		EditorGUILayout.EndHorizontal ();
		
		Normal = EditorGUILayout.Vector3Field ("Normal", Normal);
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Y Normal")) {
			pMode = PlaceMode.Vec;
			Normal = Vector3.up;
		}
		if (GUILayout.Button ("X Normal")) {
			pMode = PlaceMode.Vec;
			Normal = Vector3.right;
		}
		if (GUILayout.Button ("Z Normal")) {
			pMode = PlaceMode.Vec;
			Normal = Vector3.forward;
		}
		if (GUILayout.Button ("Camera Forward Normal")) {
			pMode = PlaceMode.Cam;
		}
		EditorGUILayout.EndHorizontal ();
		
		curve.degree = Mathf.Max (1, EditorGUILayout.IntField ("Degree",curve.degree));
		curve.loop = EditorGUILayout.Toggle ("Loop",curve.loop);
		
		if (GUILayout.Button ("Clear All Points")) {
			curve.controlPoints.Clear();
			curve.basePoints.Clear();
			curve.editPoints.Clear();
			serializedObject.UpdateIfDirtyOrScript ();
			SceneView.RepaintAll();
			
		}
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Recalculate Edit Points")) {
			curve.RecalculateEditPoints();
			SceneView.RepaintAll();
		}if (GUILayout.Button ("Recalculate Control Points")) {
			curve.RecalculateControlPoints();
			SceneView.RepaintAll();
		}
		EditorGUILayout.EndHorizontal ();
		if (sMode == SelectMode.ControlPoint) {
			SerializedProperty property = serializedObject.FindProperty ("controlPoints");
			EditorGUILayout.PropertyField (property, new GUIContent ("Points"), true);
			serializedObject.ApplyModifiedProperties ();
		}
		if (sMode == SelectMode.EditPoint) {
			SerializedProperty property = serializedObject.FindProperty ("editPoints");
			EditorGUILayout.PropertyField (property, new GUIContent ("Points"), true);
			serializedObject.ApplyModifiedProperties ();
		}
		
		
	}
	void ConstructPathFromCurve(Curve curve)
	{
		Path path = curve.GetComponent<Path> ();
		if (!path) {
			path = curve.gameObject.AddComponent<Path> ();
		}
		path.ConstructPath (curve.editPoints.ToArray ());

	}
	void ConstructMeshFromCurve(Curve curve)
	{
		MeshPath mp = new MeshPath();
		mp.Loop = curve.loop;
		mp.Shape = new Vector3[]{
			new Vector2(-2,-1),
			new Vector2(-2,1),
			new Vector2(2,1),
			new Vector2(2,-1),
		};
		
		
		MeshFilter filter = curve.GetComponent<MeshFilter> ();
		MeshCollider collider = curve.GetComponent<MeshCollider>();
		Vector3[] points;

		points = curve.editPoints.ToArray();
		
		if (filter) {
			
			if(filter.sharedMesh != null)
			{
				DestroyImmediate(filter.sharedMesh);
			}
			filter.mesh = mp.ConstructLinearPath (points);
			if(collider)
			{
				collider.sharedMesh = filter.sharedMesh;
			}
		}
	}
	void OnSceneGUI() 
	{
		if (sMode != SelectMode.Invalid) {
			if (Event.current.type == EventType.MouseDown && Event.current.button == 0 && !(Event.current.alt || Event.current.command || Event.current.control)) {
				PlacePoint (Event.current);
			}
			if (Event.current.type == EventType.Layout) {
				HandleUtility.AddDefaultControl (0);
			}
		}
	}
	void PlacePoint(Event e)
	{
		Camera cam = Camera.current;
		if(cam != null)
		{
			Curve curve = (Curve)target;
			if (pMode == PlaceMode.Cam) {
				
				Vector3 initialPoint = cam.transform.InverseTransformPoint(curve.transform.position);
				if(cam.orthographic)
				{
					Vector3 NextPoint = cam.ScreenToWorldPoint(new Vector3(e.mousePosition.x,cam.pixelHeight - e.mousePosition.y,initialPoint.z));
					NextPoint = Vector3.ProjectOnPlane(NextPoint,cam.transform.forward);
					AddPoint(NextPoint,curve);
				}
				else
				{
					Vector3 NextPoint = cam.ScreenToWorldPoint(new Vector3(e.mousePosition.x,cam.pixelHeight - e.mousePosition.y,initialPoint.z));
					AddPoint(NextPoint,curve);
				}
			}
		}
	}
	void AddPoint(Vector3 point, Curve curve)
	{
		if (sMode == SelectMode.ControlPoint) {
			curve.AddControlPoint (point);
			Repaint ();
			SceneView.RepaintAll ();
		} else if(sMode == SelectMode.EditPoint)
		{
			curve.AddEditPoint (point);
			Repaint ();
			SceneView.RepaintAll ();
		}
	}
	[DrawGizmo(GizmoType.Selected)]
	static void DrawGizmos(CurveLinear linear,GizmoType type)
	{
		if (linear.basePoints.Count > 0) {
			DrawLineHandle(linear.controlPoints.ToArray());
			Gizmos.color = Color.cyan;
			List<Vector3> points = linear.basePoints;
			Handles.color = Color.cyan;
			for (int i = 0; i < points.Count; ++i) {
				Handles.DotCap (0, points [i], Quaternion.identity, 1);
			}
		}
	}
	static void DrawCurveHandle(Curve curve)
	{
		if (curve.resolution >= 1) {
			float delta = Mathf.Max(1.0f / curve.resolution,0.00001f);
			float t = 0;
			Vector3 point1 = curve.GetPointAt (0);
			t += delta;
			while (t < 1) {
				Vector3 point2 = curve.GetPointAt (t);
				t += delta;
				Handles.DrawLine (point1, point2);
				point1 = point2;
			}
			Handles.DrawLine (point1, curve.GetPointAt (1));
		}
	}
	static void DrawLineHandle(Vector3[] points)
	{
		Vector3 p1;
		Vector3 p2;
		p1 = points [0];
		for(int i = 1; i < points.Length;++i)
		{
			p2 = points[i];
			Handles.DrawLine(p1,p2);
			p1=p2;
		}
	}
}
*/