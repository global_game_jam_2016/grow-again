﻿/*using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(Curve),true)]
[CanEditMultipleObjects]
public class CurveEditor : Editor {
	
	Vector3 Normal;
	enum PlaceMode
	{
		Vec,
		Cam
	}
	enum SelectMode
	{
		Invalid,
		ControlPoint,
		EditPoint
	}
	PlaceMode pMode = PlaceMode.Cam;
	SelectMode sMode = SelectMode.Invalid;
	public override void OnInspectorGUI()
	{
		serializedObject.UpdateIfDirtyOrScript ();
		Curve curve = (Curve)target;
		
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("ConstructPath")) {
			ConstructPathFromCurve(curve);
		}
		curve.resolution = EditorGUILayout.IntField ("Resolution", curve.resolution);
		EditorGUILayout.EndHorizontal ();
		
		EditorGUILayout.BeginHorizontal ();
		
		
		
		if (GUILayout.Button ("GenerateMesh")) {
			ConstructMeshFromCurve(curve);
		}
		curve.meshResolution = EditorGUILayout.IntField ("Resolution", curve.meshResolution);
		EditorGUILayout.EndHorizontal ();
		
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Off")) {
			sMode = SelectMode.Invalid;
		}
		if (GUILayout.Button ("Curve Point")) {
			sMode = SelectMode.ControlPoint;
		}
		if (GUILayout.Button ("Edit Point")) {
			sMode = SelectMode.EditPoint;
		}
		EditorGUILayout.EndHorizontal ();
		
		Normal = EditorGUILayout.Vector3Field ("Normal", Normal);
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Y Normal")) {
			pMode = PlaceMode.Vec;
			Normal = Vector3.up;
		}
		if (GUILayout.Button ("X Normal")) {
			pMode = PlaceMode.Vec;
			Normal = Vector3.right;
		}
		if (GUILayout.Button ("Z Normal")) {
			pMode = PlaceMode.Vec;
			Normal = Vector3.forward;
		}
		if (GUILayout.Button ("Camera Forward Normal")) {
			pMode = PlaceMode.Cam;
		}
		EditorGUILayout.EndHorizontal ();
		
		curve.degree = Mathf.Max (1, EditorGUILayout.IntField ("Degree",curve.degree));
		curve.loop = EditorGUILayout.Toggle ("Loop",curve.loop);
		
		if (GUILayout.Button ("Clear All Points")) {
			curve.controlPoints.Clear();
			curve.basePoints.Clear();
			curve.editPoints.Clear();
			serializedObject.UpdateIfDirtyOrScript ();
			SceneView.RepaintAll();
			
		}
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Recalculate Edit Points")) {
			curve.RecalculateEditPoints();
			SceneView.RepaintAll();
		}if (GUILayout.Button ("Recalculate Control Points")) {
			curve.RecalculateControlPoints();
			SceneView.RepaintAll();
		}
		EditorGUILayout.EndHorizontal ();
		if (sMode == SelectMode.ControlPoint) {
			SerializedProperty property = serializedObject.FindProperty ("controlPoints");
			EditorGUILayout.PropertyField (property, new GUIContent ("Points"), true);
			serializedObject.ApplyModifiedProperties ();
		}
		if (sMode == SelectMode.EditPoint) {
			SerializedProperty property = serializedObject.FindProperty ("editPoints");
			EditorGUILayout.PropertyField (property, new GUIContent ("Points"), true);
			serializedObject.ApplyModifiedProperties ();
		}
		
		
	}
	void ConstructPathFromCurve(Curve curve)
	{
		Path path = curve.GetComponent<Path> ();
		if (!path) {
			path = curve.gameObject.AddComponent<Path> ();
		}
		if (curve.degree > 1) {
			Vector3[] points = new Vector3[curve.resolution+1];
			if (curve.resolution >= 1) {
				float delta = Mathf.Max(1.0f / (curve.resolution+1),0.00001f);
				Handles.color = Color.cyan;
				float t = 0;
				for(int i = 0; i < curve.resolution+1;++i)
				{
					points[i] = curve.GetPointAt (t);
					t += delta;
				}
				points[curve.resolution] = curve.GetPointAt(1);
				path.ConstructPath(points);
			}
		} else {
			path.ConstructPath (curve.editPoints.ToArray ());
		}
	}
	void ConstructMeshFromCurve(Curve curve)
	{
		MeshPath mp = new MeshPath();
		mp.Loop = curve.loop;
		mp.Shape = new Vector3[]{
			new Vector2(-2,-1),
			new Vector2(-2,1),
			new Vector2(2,1),
			new Vector2(2,-1),
		};
		
		
		MeshFilter filter = curve.GetComponent<MeshFilter> ();
		MeshCollider collider = curve.GetComponent<MeshCollider>();
		Vector3[] points;
		if (curve.degree > 1) {
			points = new Vector3[curve.meshResolution + 1];
			if (curve.meshResolution >= 1) {
				float delta = Mathf.Max (1.0f / (curve.meshResolution + 1), 0.00001f);
				Handles.color = Color.cyan;
				float t = 0;
				for (int i = 0; i < curve.meshResolution+1; ++i) {
					points [i] = curve.GetPointAt (t);
					t += delta;
				}
				points [curve.meshResolution] = curve.GetPointAt (1);
				mp.ConstructLinearPath (points);
			}
		} else {
			points = curve.editPoints.ToArray();
		}
		
		
		
		if (filter) {
			
			if(filter.sharedMesh != null)
			{
				DestroyImmediate(filter.sharedMesh);
			}
			filter.mesh = mp.ConstructLinearPath (points);
			if(collider)
			{
				collider.sharedMesh = filter.sharedMesh;
			}
		}
	}
	void OnSceneGUI() 
	{
		
		DrawCurve ();
		if (sMode != SelectMode.Invalid) {
			if (Event.current.type == EventType.MouseDown && Event.current.button == 0 && !(Event.current.alt || Event.current.command || Event.current.control)) {
				PlacePoint (Event.current);
			}
			if (Event.current.type == EventType.Layout) {
				HandleUtility.AddDefaultControl (0);
			}
		}
	}
	void PlacePoint(Event e)
	{
		Camera cam = Camera.current;
		if(cam != null)
		{
			Curve curve = (Curve)target;
			if (pMode == PlaceMode.Cam) {
				
				Vector3 initialPoint = cam.transform.InverseTransformPoint(curve.transform.position);
				if(cam.orthographic)
				{
					Vector3 NextPoint = cam.ScreenToWorldPoint(new Vector3(e.mousePosition.x,cam.pixelHeight - e.mousePosition.y,initialPoint.z));
					NextPoint = Vector3.ProjectOnPlane(NextPoint,cam.transform.forward);
					AddPoint(NextPoint,curve);
				}
				else
				{
					Vector3 NextPoint = cam.ScreenToWorldPoint(new Vector3(e.mousePosition.x,cam.pixelHeight - e.mousePosition.y,initialPoint.z));
					AddPoint(NextPoint,curve);
				}
			}
		}
	}
	void AddPoint(Vector3 point, Curve curve)
	{
		if (sMode == SelectMode.ControlPoint) {
			curve.AddControlPoint (point);
			Repaint ();
			SceneView.RepaintAll ();
		} else if(sMode == SelectMode.EditPoint)
		{
			curve.AddEditPoint (point);
			Repaint ();
			SceneView.RepaintAll ();
		}
	}
	
	#if UNITY_EDITOR
	void DrawCurve()
	{
		Curve curve = (Curve)target;
		
		
		
		if (curve.controlPoints.Count > 1) {
			Handles.color = Color.magenta;
			foreach (Vector3 p in curve.basePoints) {
				Handles.DotCap (0, p, Quaternion.identity, 2);
			}
			
			
			
			Handles.color = Color.yellow;
			foreach (Vector3 p in curve.controlPoints) {
				Handles.DotCap (0, p, Quaternion.identity, 1);
			}
			//DrawLineHandle(curve.controlPoints.ToArray());
			if(curve.loop)
			{
				Handles.color = Color.green;
				Handles.DrawLine(curve.controlPoints[0],curve.controlPoints[curve.controlPoints.Count-1]);
			}
		}
		if (curve.editPoints.Count > 1) {
			Handles.color = Color.cyan;
			foreach (Vector3 p in curve.editPoints) {
				Handles.DotCap (0, p, Quaternion.identity, 1);
			}
			if(curve.loop)
			{
				Handles.color = Color.red;
				Handles.DrawLine(curve.editPoints[0],curve.editPoints[curve.editPoints.Count-1]);
			}
		}
		Handles.color = Color.cyan;
		if(curve.basePoints.Count > curve.degree)
		{
			if(curve.degree > 1)
			{
				DrawCurveHandle(curve);
			}
			else
			{
				DrawLineHandle(curve.editPoints.ToArray());
			}
			
		}
		
	}
	void DrawCurveHandle(Curve curve)
	{
		if (curve.resolution >= 1) {
			float delta = Mathf.Max(1.0f / curve.resolution,0.00001f);
			float t = 0;
			Vector3 point1 = curve.GetPointAt (0);
			t += delta;
			while (t < 1) {
				Vector3 point2 = curve.GetPointAt (t);
				t += delta;
				Handles.DrawLine (point1, point2);
				point1 = point2;
			}
			Handles.DrawLine (point1, curve.GetPointAt (1));
		}
	}
	void DrawLineHandle(Vector3[] points)
	{
		Vector3 p1;
		Vector3 p2;
		p1 = points [0];
		for(int i = 1; i < points.Length;++i)
		{
			p2 = points[i];
			Handles.DrawLine(p1,p2);
			p1=p2;
		}
	}
	#endif
}*/
