﻿using UnityEngine;
using System.Collections;

public class CurveLinear : Curve {

	public override void RecalculateControlPoints ()
	{
		controlPoints = editPoints;
		RecalculateBasePoints ();
	}
	public override void RecalculateEditPoints ()
	{
		editPoints = controlPoints;
		RecalculateBasePoints ();
	}
	public override void RecalculateBasePoints ()
	{
		basePoints = editPoints;
	}
}
