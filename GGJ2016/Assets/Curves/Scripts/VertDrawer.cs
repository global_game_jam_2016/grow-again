﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
public class VertDrawer : MonoBehaviour {

	public Color color = Color.white;
	void OnDrawGizmos()
	{
		Handles.color = color;
		MeshFilter filter = GetComponent<MeshFilter> ();
		if (filter) {
			foreach (Vector3 point in filter.sharedMesh.vertices) {
				
				Handles.DotCap (0,point,Quaternion.identity,0.2f);
			}
		}
	}
}
#endif