﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct PathNode
{
	public Vector3 Position;
	public Quaternion Direction;
	public Vector3 ProjPosition
	{
		get
		{
			Vector3 v = Position;
			v.y = 0;
			return v;
		}
	}
}
public class Path : MonoBehaviour {
	public bool ForceFollow = true; 
	public bool loop = true;
	public PathNode[] Nodes;

	public void ConstructPath(Vector3[] Points)
	{
		int segments = Points.Length;
		Nodes = new PathNode[segments];
		Quaternion[] quats = GenerateDirections (Points);
		for(int i = 0; i < segments; ++i)
		{
			Nodes[i].Position = Points[i];
			Nodes[i].Direction = quats[i];
		}
	}
	public Vector3 GetClosestPoint(Vector3 point,int index)
	{
		Vector3 v1 = Nodes [index].Position;
		Vector3 v2 = Nodes [(index + 1) % Nodes.Length].Position;
		
		Vector3 v1P = point - v1;
		Vector3 v1v2 = v2 - v1;
		
		float v1v22 = v1v2.sqrMagnitude;
		
		float dot = Vector3.Dot (v1P,v1v2);
		float t = dot / v1v22;
		return Vector3.Lerp (v1, v2, t);
	}
	public int GetClosestIndex(Vector3 point)
	{
		int c = Nodes.Length;
		if(c > 0)
		{
			float minDist = GetDistanceToClosest(point,0);
			int index = 0;
			for(int i = 0; i < Nodes.Length ;++i)
			{
				float dist = GetDistanceToClosest(point,i);
				if(dist < minDist)
				{
					minDist = dist;
					index = i;
				}
			}
			return index;
		}
		return 0;
	}
	public float GetDistanceToClosest(Vector3 point,int index)
	{
		return Vector3.Distance(point,GetClosestPoint(point,index));
	}
	public Vector3 GetClosestProjPoint(Vector3 point,int index)
	{
		Vector3 v1 = Nodes [index].ProjPosition;
		Vector3 v2 = Nodes [(index + 1) % Nodes.Length].ProjPosition;
		
		Vector3 v1P = point - v1;
		Vector3 v1v2 = v2 - v1;
		
		float v1v22 = v1v2.sqrMagnitude;
		
		float dot = Vector3.Dot (v1P,v1v2);
		float t = dot / v1v22;
		return Vector3.Lerp (v1, v2, t);
	}
	public float GetProjTvalue(Vector3 point,int index)
	{
		Vector3 v1 = Nodes [index].ProjPosition;
		Vector3 v2 = Nodes [(index + 1) % Nodes.Length].ProjPosition;
		
		Vector3 v1P = point - v1;
		Vector3 v1v2 = v2 - v1;
		
		float v1v22 = v1v2.sqrMagnitude;
		
		float dot = Vector3.Dot (v1P,v1v2);
		float t = dot / v1v22;
		return t;
	}


	private Quaternion[] GenerateDirections (Vector3[] Points)
	{
		
		int segments = Points.Length;
		Quaternion[] quats = new Quaternion[segments];
		
		//First point
		if (loop) {
			Vector3 v1 = Points[segments-1]-Points[0];
			Vector3 v2 = Points[0]-Points[1];
			quats[0] =Quaternion.LookRotation(Vector3.Lerp(v1,v2,0.5f));
		} else {
			Vector3 v1 = Points[0]-Points[1];
			quats [0] = Quaternion.LookRotation(v1);
		}
		
		for (int i = 1; i < segments - 1; ++i) {
			Vector3 v1 = Points[i-1]-Points[i];
			Vector3 v2 = Points[i]-Points[i+1];
			quats[i] =Quaternion.LookRotation(Vector3.Lerp(v1,v2,0.5f));
		}
		//Last Point
		if (loop) {
			Vector3 v1 = Points[segments-2]-Points[segments-1];
			Vector3 v2 = Points[segments-1]-Points[0];
			quats[segments - 1] =Quaternion.LookRotation(Vector3.Lerp(v1,v2,0.5f));
		} else {
			Vector3 v1 = Points[Points.Length-2]-Points[Points.Length-1];
			quats [segments - 1] = Quaternion.LookRotation(v1);
		}
		return quats;
	}
}