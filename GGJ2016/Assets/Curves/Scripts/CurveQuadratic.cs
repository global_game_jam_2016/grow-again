﻿using UnityEngine;
using System.Collections;

public class CurveQuadratic : Curve
{
	public override void RecalculateEditPoints()
	{
		RecalculateBasePoints ();
		if (controlPoints.Count > degree) {
			int c = controlPoints.Count - 1;

				editPoints.Clear();
				editPoints.Capacity = c;
				
				editPoints.Add(controlPoints[0]);
				for (int i = (1); i < c-(1); ++i) {
					Vector3	ep = Vector3.Lerp(controlPoints[i],controlPoints[i+1],0.5f);
					editPoints.Add(ep);
				}
				editPoints.Add(controlPoints[c]);
			
		}
	}

	public override void RecalculateControlPoints()
	{
		if (editPoints.Count > 2) {
			int c = editPoints.Count + 1;
				controlPoints.Clear ();
				controlPoints.Capacity = c;
				controlPoints.Add(editPoints[0]);
				controlPoints.Add(Vector3.Lerp(editPoints[0],editPoints[1],0.5f));
				for(int i = 1; i < c-2;++i)
				{
					Vector3 v1 = editPoints[i];
					Vector3 v2 = controlPoints[i];
					controlPoints.Add(v1 + (v1 - v2));
				}
				
				controlPoints.Add(editPoints[c-2]);
				basePoints = controlPoints;
		}
	}
	public override void RecalculateBasePoints()
	{
		int cpCount = controlPoints.Count;
		int epCount = (controlPoints.Count - 1);
		int cap = epCount *(1);
		if (cpCount > 2) {
			basePoints = controlPoints;	
		}
	}
}
