﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

// https://www.particleincell.com/2012/bezier-splines/#demo
// http://paulbourke.net/miscellaneous/interpolation/

public class Curve : MonoBehaviour 
{
	protected List<Vector3> Points = new List<Vector3>();
	public List<Vector3> basePoints = new List<Vector3>();
	public List<Vector3> controlPoints = new List<Vector3>();
	public List<Vector3> editPoints = new List<Vector3>();
	public int degree;
	public bool loop;
	public int resolution = 1;
	public int meshResolution = 1;

	public void AddControlPoint(Vector3 point)
	{
		controlPoints.Add (point);
		RecalculateEditPoints ();

	}
	public void AddEditPoint(Vector3 point)
	{
		editPoints.Add (point);
		RecalculateControlPoints ();
	}
	public virtual  void RecalculateEditPoints()
	{
		RecalculateBasePoints ();
		if (controlPoints.Count > degree) {
			int c = controlPoints.Count - (degree-1);
			if (degree == 2) {
				editPoints.Clear();
				editPoints.Capacity = c;
				
				editPoints.Add(controlPoints[0]);
				for (int i = (degree-1); i < c-(degree-1); ++i) {
					Vector3	ep = Vector3.Lerp(controlPoints[i],controlPoints[i+1],0.5f);
					editPoints.Add(ep);
				}
				editPoints.Add(controlPoints[c]);

			}

			if(degree == 3)
			{
				editPoints.Clear();
				editPoints.Capacity = c;

				editPoints.Add(controlPoints[0]);
				for (int i = 1; i < c-1; ++i) {
					Vector3	ep = Vector3.Lerp(basePoints[i*2],basePoints[i*2+1],0.5f);
					editPoints.Add(ep);
				}
				editPoints.Add(controlPoints[c+(degree-2)]);
			}

		}
	}
	public virtual  void RecalculateBasePoints()
	{
		int cpCount = controlPoints.Count;
		int epCount = (controlPoints.Count - (degree-1));
		int cap = epCount *(degree-1);
		if(cpCount > degree)
		{
			if(degree < 3)
			{
				basePoints = controlPoints;
			}
			if(degree == 3)
			{
				basePoints.Clear();
				basePoints.Capacity = cap;


				for(int i1 = 0; i1 < 2;++i1)
				{
					basePoints.Add(controlPoints[i1]);
				}
				if(cpCount > 4)
				{
					basePoints.Add(Vector3.Lerp(controlPoints[1],controlPoints[2],0.5f));
				}
				for(int i2 = 2; i2 < cpCount -3;++i2)
				{
					Vector3 v1 = Vector3.Lerp(controlPoints[i2],controlPoints[i2+1],1.0f/3.0f);
					Vector3 v2 = Vector3.Lerp(controlPoints[i2],controlPoints[i2+1],2.0f/3.0f);
					basePoints.Add(v1);
					basePoints.Add(v2);
				}
				if(cpCount > 4)
				{
					basePoints.Add(Vector3.Lerp(controlPoints[cpCount-2],controlPoints[cpCount-3],0.5f));
				}
				for(int i3 = 2; i3 > 0;--i3)
				{
					basePoints.Add(controlPoints[cpCount-i3]);
				}
			}
			else
			{
				basePoints.Clear();
				basePoints.Capacity = cap;
				
				
				for(int i1 = 0; i1 < degree-1;++i1)
				{
					basePoints.Add(controlPoints[i1]);
				}
				for(int i2 = 2; i2 < cpCount -3;++i2)
				{
					Vector3 v1 = Vector3.Lerp(controlPoints[i2],controlPoints[i2+1],1.0f/3.0f);
					Vector3 v2 = Vector3.Lerp(controlPoints[i2],controlPoints[i2+1],2.0f/3.0f);
					basePoints.Add(v1);
					basePoints.Add(v2);
				}
				if(cpCount > 4)
				{
					basePoints.Add(Vector3.Lerp(controlPoints[cpCount-2],controlPoints[cpCount-3],0.5f));
				}
				for(int i3 = degree-1; i3 > 0;--i3)
				{
					basePoints.Add(controlPoints[cpCount-i3]);
				}
			}
		}
	}
	public virtual  void RecalculateControlPoints()
	{
		if (editPoints.Count > degree) {
			int c = editPoints.Count + 1;
			if (degree == 2) {
				controlPoints.Clear ();
				controlPoints.Capacity = c;
				controlPoints.Add(editPoints[0]);
				controlPoints.Add(Vector3.Lerp(editPoints[0],editPoints[1],0.5f));
				for(int i = 1; i < c-2;++i)
				{
					Vector3 v1 = editPoints[i];
					Vector3 v2 = controlPoints[i];
					controlPoints.Add(v1 + (v1 - v2));
				}

				controlPoints.Add(editPoints[c-2]);
				basePoints = controlPoints;
			}
		}
	}
	public virtual  Vector3 GetPointAt(float t)
	{
		int segments = editPoints.Count-1;//((controlPoints.Count - 1)/degree);
		t = t * (segments);
		int i = 0;
		while (t > 1) {
			--t;
			++i;
		}
		return Depth(t, 0,i);
	}
	Vector3 Depth(float t, int depthIndex, int offset = 0)
	{
		int dI = depthIndex;
		int n = degree;
		Vector3 v;
		if (depthIndex % degree == 0) {
			int c = depthIndex / degree;
			v = editPoints [c + (offset)];
		} else {
			v = basePoints [depthIndex + (offset * (degree-1))];
		}
		
		//v = controlPoints [depthIndex + (offset * degree)];
		if (depthIndex < 0 || depthIndex > degree)
		{
			throw new System.IndexOutOfRangeException();
		}

		if (depthIndex < degree)
		{
			return MathHelper.GetBinCoeff(n,dI) * Mathf.Pow(1f - t, n - dI) * Mathf.Pow(t, dI) * v + Depth(t, dI+1,offset);
		}
		return MathHelper.GetBinCoeff(n,dI) * Mathf.Pow(1f - t, n - dI) * Mathf.Pow(t, dI) * v;
	}
}
