﻿using UnityEngine;
using System.Collections;

public class CurveCubic : Curve {

	public override void RecalculateControlPoints ()
	{
		/*if (editPoints.Count > degree) {
			int c = editPoints.Count + 1;
			if (degree == 2) {
				controlPoints.Clear ();
				controlPoints.Capacity = c;
				controlPoints.Add(editPoints[0]);
				controlPoints.Add(Vector3.Lerp(editPoints[0],editPoints[1],0.5f));
				for(int i = 1; i < c-2;++i)
				{
					Vector3 v1 = editPoints[i];
					Vector3 v2 = controlPoints[i];
					controlPoints.Add(v1 + (v1 - v2));
				}
				
				controlPoints.Add(editPoints[c-2]);
				basePoints = controlPoints;
			}
		}*/
	}
	public override void RecalculateEditPoints ()
	{
		if (controlPoints.Count > degree) {
			RecalculateBasePoints ();
			int c = controlPoints.Count - (2);
			editPoints.Clear ();
			//editPoints.Capacity = c;
			editPoints.Add (controlPoints [0]);
			for (int i = 1; i < c-1; ++i) {
				Vector3 ep = Vector3.Lerp (basePoints [i * 2], basePoints [i * 2 + 1], 0.5f);
				editPoints.Add (ep);
			}
			editPoints.Add (controlPoints [c + 1]);
		}
	}
	public override void RecalculateBasePoints ()
	{
		int cpCount = controlPoints.Count;
		int epCount = (controlPoints.Count - (degree-1));
		int cap = epCount *(degree-1);

		basePoints.Clear();
		//basePoints.Capacity = cap;
		
		
		for(int i1 = 0; i1 < 2;++i1)
		{
			basePoints.Add(controlPoints[i1]);
		}
		if(cpCount > 4)
		{
			basePoints.Add(Vector3.Lerp(controlPoints[1],controlPoints[2],0.5f));
		}
		for(int i2 = 2; i2 < cpCount -3;++i2)
		{
			Vector3 v1 = Vector3.Lerp(controlPoints[i2],controlPoints[i2+1],1.0f/3.0f);
			Vector3 v2 = Vector3.Lerp(controlPoints[i2],controlPoints[i2+1],2.0f/3.0f);
			basePoints.Add(v1);
			basePoints.Add(v2);
		}
		if(cpCount > 4)
		{
			basePoints.Add(Vector3.Lerp(controlPoints[cpCount-2],controlPoints[cpCount-3],0.5f));
		}
		for(int i3 = 2; i3 > 0;--i3)
		{
			basePoints.Add(controlPoints[cpCount-i3]);
		}
	}
}
