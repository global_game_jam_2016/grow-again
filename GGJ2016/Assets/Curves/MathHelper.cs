﻿using UnityEngine;
using System.Collections;

public class MathHelper : MonoBehaviour {

	public static long GetBinCoeff(long N, long K)
	{
		// This function gets the total number of unique combinations based upon N and K.
		// N is the total number of items.
		// K is the size of the group.
		// Total number of unique combinations = N! / ( K! (N - K)! ).
		// This function is less efficient, but is more likely to not overflow when N and K are large.
		// Taken from:  http://blog.plover.com/math/choose.html
		//
		long r = 1;
		long d;
		if (K > N) return 0;
		for (d = 1; d <= K; d++)
		{
			r *= N--;
			r /= d;
		}
		return r;
	}
	public static Vector3 ReverseLerp(Vector3 rhs, Vector3 lhs, float t)
	{
		//return Vector3 = (1-t) * rhs + t * lhs;
		return (lhs - ((1-t) * rhs))/t;

	}
}
